import React from "react";
import {Navbar,Nav} from "react-bootstrap";
import {credfine} from "../Pages/HomeMap"

const Header=()=>{
    return(<>
      <div className="container-fluid shadowClass">
        <div className="navwidth">
        <Navbar expand="lg">
				<Navbar.Brand href="/" className="m-0"><img src={credfine[0].logoCredfine} alt="logo" className="logosvg"/></Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<Nav.Link href="/">Home</Nav.Link>
					<div className="nav-item dropdown">
                        <Nav.Link className="nav-link" href="/personalLoan">
                        Personal Loan <i className="fa fa-caret-down" aria-hidden="true"></i>
                        </Nav.Link>
                        <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                        <Nav.Link className="dropdown-item dropdn_font" href="">Salaried</Nav.Link>
                        <Nav.Link className="dropdown-item dropdn_font" href="">Self-Employed<br/> Professional</Nav.Link>
                        <Nav.Link className="dropdown-item dropdn_font" href="">Self-Employed<br/> Business</Nav.Link>
                        </div>
                  </div>

                  <div className="nav-item dropdown">
                <Nav.Link className="nav-link" href="/CreditCard">
                  Credit Card <i className="fa fa-caret-down" aria-hidden="true"></i>
                </Nav.Link>
                <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                  <Nav.Link className="dropdown-item dropdn_font" href="">Credit Card</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="">Credit Card</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="">Credit Card</Nav.Link>
                </div>
              </div>

              <div className="nav-item dropdown">
                <Nav.Link className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Loans 
                </Nav.Link>
                <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                  <Nav.Link className="dropdown-item dropdn_font" href="/BusinessLoan">Business Loan </Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="/HomeLoan">Home Loan</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="/LoanAgainstProperty">Loan against Property</Nav.Link>
                </div>
              </div>

              <div className="nav-item dropdown">
                <Nav.Link className="nav-link" href="/InsurancePlans">
                  Insurance <i className="fa fa-caret-down" aria-hidden="true"></i>
                </Nav.Link>
                <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Health Insurance</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Life Insurance</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Motor Insurance</Nav.Link>
                </div>
              </div>

              <div className="nav-item dropdown">
                <Nav.Link className="nav-link" href="/BalanceTransfer">
                  Balance Transfer <i className="fa fa-caret-down" aria-hidden="true"></i>
                </Nav.Link>
                <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Balance Transfer</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Balance Transfer</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Balance Transfer</Nav.Link>
                </div>
              </div>

              <div className="nav-item dropdown">
                <Nav.Link className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Calculators 
                </Nav.Link>
                <div className="dropdown-menu drop_mainShadow" aria-labelledby="navbarDropdown">
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Calculators</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Calculators</Nav.Link>
                  <Nav.Link className="dropdown-item dropdn_font" href="#">Calculators</Nav.Link>
                </div>
              </div>
            </Nav>

            <button className="btn btn_loan "> CUSTOMER LOGIN  </button>
					
				</Navbar.Collapse>
			</Navbar> 
        </div>
      </div>
    </>)
};
export default Header;