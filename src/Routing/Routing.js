import React from "react"
import {Redirect, Route,Switch } from "react-router-dom";
import Header from "./Header";
import Home from "../Pages/Home";
import PersonalLoan from "../Pages/PersonalLoan";
import CreditCard from "../Pages/CreditCard";
import BusinessLoan from "../Pages/BusinessLoan";
import HomeLoan from "../Pages/HomeLoan";
import LoanAgainstProperty from "../Pages/LoanAgainstProperty";
import InsurancePlans from "../Pages/InsurancePlans";
import BalanceTransfer from "../Pages/BalanceTransfer";
import AboutUs from "../Pages/AboutUs";
import OurTeam from "../Pages/OurTeam";
import OurPartners from "../Pages/OurPartners";
import Career from "../Pages/Career";




const Routing=()=>{
    return(<>
        <Header/>
        <Switch>
            <Route exact path="/" render={()=>{return(<><Home/></>)}}/>
            <Route exact path="/personalLoan" render={()=>{return(<><PersonalLoan/></>)}}/>
            <Route exact path="/CreditCard" render={()=>{return(<><CreditCard/></>)}}/>
            <Route exact path="/BusinessLoan" render={()=>{return(<><BusinessLoan/></>)}}/>
            <Route exact path="/HomeLoan" render={()=>{return(<><HomeLoan/></>)}}/>
            <Route exact path="/LoanAgainstProperty" render={()=>{return(<><LoanAgainstProperty/></>)}}/>
            <Route exact path="/InsurancePlans" render={()=>{return(<><InsurancePlans/></>)}}/>
            <Route exact path="/BalanceTransfer" render={()=>{return(<><BalanceTransfer/></>)}}/>
            <Route exact path="/AboutUs" render={()=>{return(<><AboutUs/></>)}}/>
            <Route exact path="/OurTeam" render={()=>{return(<><OurTeam/></>)}}/>
            <Route exact path="/OurPartners" render={()=>{return(<><OurPartners/></>)}}/>
            <Route exact path="/Career" render={()=>{return(<><Career/></>)}}/>

            <Redirect to="/" exact/>
        </Switch>
    </>)
};
export default Routing;