import React from "react";
import { NavLink } from "react-router-dom";

const Header2=(props)=>{
    return(<>
    <div className="aboutMain">
        <div className="aboutContainer"></div>
        <div className="tabDiv">
            <nav className="navbar navbar-default">
              <ul className="flex-tab">
                <li className="active">
                <NavLink activeClassName="activehead2" exact to="/AboutUs">About us</NavLink>
                </li>
                <li><NavLink activeClassName="activehead2" exact to="/OurTeam">our team</NavLink></li>
                <li><NavLink activeClassName="activehead2" exact to="/OurPartners">our partners</NavLink></li>
                <li><NavLink activeClassName="activehead2" exact to="/Career">career</NavLink></li>
              </ul>
              </nav>
        </div>
        <div className="aboutImg">

        <img src={props.imgsrc1} class="aboutImg1" alt="txt"/>
        <img src={props.imgsrc2} class="aboutImg2" alt="txt"/>

</div>

</div>
    </>)
};
export default Header2;