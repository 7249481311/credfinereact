import React from "react";
import {FooterSvg} from "../Pages/HomeMap";

const Footer=()=>{
    return(<>
        <footer>
  <div className="footerClassnw">

  <div className="text-center socialIconfooter">
       

       {FooterSvg.map((data,index)=>{
           return(<>
            <a href="" className="footerSvg" key={index}>
                <img src={data.svg} alt="footer social icon"/>
            </a> 
           </>)
       })}

       

    </div>

    <div className="container">
    <div className="row">
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Our Products</li>
            <li className="listInitem">Personal Loan</li>
            <li className="listInitem">Business Loan</li>
            <li className="listInitem">Credit Cards</li>
            <li className="listInitem">Home Loan</li>
            <li className="listInitem">Mortgage Loan</li>
            <li className="listInitem">Balance Transfer</li>
            <li className="listInitem">Insurance Plans</li>
          </ul>
        </div>

        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">CredFine</li>
            <li className="listInitem">About Us</li>
            <li className="listInitem">Our Team</li>
            <li className="listInitem">Our Partners</li>
            <li className="listInitem">Career</li>
            <li className="listInitem">Blog</li>
            <li className="listInitem">Press &amp; Media</li>
            <li className="listInitem">Refer &amp; Win</li>
          </ul>
        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Partner with us</li>
            <li className="listInitem">Signup as Partner</li>
            <li className="listInitem">Partner Benefits</li>
          </ul>

        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Help</li>
            <li className="listInitem">FAQs</li>
            <li className="listInitem">Ask an Expert</li>
            <li className="listInitem">Calculators</li>
          </ul>
        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Policies</li>
            <li className="listInitem">Terms &amp; Conditions</li>
            <li className="listInitem">Privacy Policy</li>
            <li className="listInitem">Security Policy</li>
            <li className="listInitem">Disclaimer</li>

            <a href="" className="btn partnerloginbtn uppercase">partner login</a>
          </ul>
        </div>
        
      </div>

      <hr className="footrhr"/>

      <div className="row">
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Personal Loan</li>
            <li className="listInitem">HDFC Bank</li>
            <li className="listInitem">Kotak Bank</li>
            <li className="listInitem">ICICI Bank</li>
            <li className="listInitem">Axis Bank</li>
            
          </ul>
        </div>

        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Home Loan</li>
            <li className="listInitem">SBI Home Loan</li>
            <li className="listInitem">HDFC Home Loan</li>
            <li className="listInitem">Axis Home Loan</li>
            <li className="listInitem">Citi Home Loan</li>

          </ul>
        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Business Loan</li>
            <li className="listInitem">SBI Home Loan</li>
            <li className="listInitem">HDFC Home Loan</li>
            <li className="listInitem">Axis Home Loan</li>
            <li className="listInitem">Citi Home Loan</li>
          </ul>

        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Credit Cards</li>
            <li className="listInitem">IndusInd Credit Card</li>
            <li className="listInitem">SBI Credit Card</li>
            <li className="listInitem">ICICI Credit Card</li>
            <li className="listInitem">Kotak Credit Card</li>
          </ul>
        </div>
        <div className="col">
          <ul className="footer_ul">
            <li className="listHeadF">Insurance</li>
            <li className="listInitem">Met Life</li>
            <li className="listInitem">Bajaj Allianz</li>
            <li className="listInitem">Aviva Insurance</li>
            <li className="listInitem">Bharti AXA</li>
          </ul>
        </div>
        
      </div>


  </div>  
  </div>  


  <div className="lstfooterwidth">
    <div className="lstfooterflex">
      <p className="contentOnlstfot mb-0">Copyright 2019 © Inobela Finanza. <span className="font400">All Rights Reserved</span></p>
      <p className="contentOnlstfot mb-0"><span className="font400">Design + Tech</span> <a href="">Appsomeness</a></p>
    </div>
  </div>

</footer>    
    </>);
};
export default Footer;