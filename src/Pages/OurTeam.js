import React from "react";
import {AboutImg1,TeamImage,TeamImage2,TeamImage3} from "./HomeMap";
import Header2 from "../Routing/Header2";
import Management from "./OurTeamComp/Management";
import Footer from "../Routing/Footer";

const OurTeam=()=>{
    return(<>
<section>
     <Header2 imgsrc1={AboutImg1[2].Img} imgsrc2={AboutImg1[1].Img}/>
</section>

<section>
    <div className="aboutContent OurTeamContent">
        <div className="container">
            <h1 className="">Management</h1>
            <div className="row m-0">
              {TeamImage.map((data,index)=>{
                  return(<>
                    <Management key={index} imgsrc={data.TeamImg} svg1={data.linkedin} svg2={data.twitter}/>
                  </>)
              })}
               
            </div>
            <br/>
            <h1 className="leadership">Leadership Team &amp; Advisors</h1>
            <div className="row m-0">
              
            {TeamImage2.map((data,index)=>{
                  return(<>
                    <Management key={index} imgsrc={data.TeamImg} svg1={data.linkedin} svg2={data.twitter}/>
                  </>)
              })}
            </div>
            <br/><br/>
            <div className="row m-0">
              
                {TeamImage3.map((data,index)=>{
                    return(<>
                        <Management key={index} imgsrc={data.TeamImg} svg1={data.linkedin} svg2={data.twitter}/>
                    </>)
                })}
            </div>

        </div>
    </div>

</section>

<section>
    <div className="container">
        <div className="italictex">
            <i>Teamwork is the ability to work together toward <br/> a common vision…</i>
        </div>
    </div>
</section>

<Footer/>

    </>)
}
export default OurTeam;