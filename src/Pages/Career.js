import React, { useEffect, useState } from "react";
import Header2 from "../Routing/Header2";
import {AboutImg1,MailIcon,careerSlider} from "./HomeMap";
import {Carousel} from "react-bootstrap";
import Footer from "../Routing/Footer";


const Career=()=>{

  const [newfront,setnewfont]=useState({});
  

  useEffect(()=>{
    const x = window.matchMedia("(min-width: 1680px)");
    if (x.matches) { 
      setnewfont({
        fontSize:"23px",
      });
    };
  },[])

    return(<>
        <section>
            <Header2 imgsrc1={AboutImg1[5].Img} imgsrc2={AboutImg1[6].Img}/>
        </section>

        <section>
    <div class="aboutContent OurTeamContent">
        <div class="container">
          
                <h1 class="">Current Openings</h1>
            <div class="row">
                <div class="col-md-3">
                    
                    <div class="currentFinance">
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome" style={newfront}>Finance Specialist</h5>
                        <p class="mt-2 mb-0"><small class="pnt_threestke">
                            4 - 6 Yrs Experience</small></p>
                    </div>

                    <div class="currentFinance" style={{background: "#F8F8F8", color: "black"}}>
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>Lending Manager</h5>
                        <p class="mt-2 mb-0"><small class="pnt_threestke">
                            1 - 2 Yrs Experience</small></p>
                    </div>

                    <div class="currentFinance" style={{background: "#F8F8F8", color: "black"}}>
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>Business Manager</h5>
                        <p class="mt-2 mb-0"><small class="pnt_threestke">
                            3 - 4 Yrs Experience</small></p>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="manager">
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>Job Description - Finance Specialist</h5>

                        <p class="mt-2 mb-0"><small class="pnt_threestke">
                            We’re looking for someone to help us analyze our financial data
                             and help us make smart and <br/> strategic business decisions. You’ll
                              be a key member of the team as you’ll demonstrate your <br/> attention
                               to detail and focus on strategy through your analysis, research, 
                               and recommendations. <br/> Our ideal candidate enjoys individual work as
                                well as team projects. Sound like you?    
                        </small></p>
                        <br/><br/>
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>Roles and Responsibilities</h5>
                        
                       <ul style={{marginLeft: "1rem"}} class="mt-3">
                           <li>
                             Perform Telephonic PD/verification checks for all proposals or
                              applicants . CAM preparation and recommendations of loans and
                               obtaining sanction
                           </li>

                           <li>
                             Provide recommendations, analysis and findings to managers, for
                              sanction / rejection credit applications with proper justification
                               tied to analysis and assessment of credit risk in accordance with 
                               the credit policies.
                           </li>

                           <li>
                             Ensuring adherence to internal credit policies, RBI guidelines and
                              guidelines specific to financial partners
                           </li>

                           <li>
                             Timely processing of all proposals to ensure faster TAT
                           </li>

                           <li>
                             Create effective reports and provide feedback on a weekly basis
                           </li>

                           <li>
                             Understanding of KYC framework, documentary checks, fraud checks
                             and other lending systems
                           </li>
                       </ul>

                       <br/><br/>
                       <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>What would make you successful in this role</h5>
                       
                      <ul style={{marginLeft: "1rem"}} class="mt-3">
                          <li>
                             High comfort level with numbers 
                           
                          </li>

                          <li>
                             Strong Credit analytical skills 
                            
                          </li>

                          <li>
                             Ability to research and collate different sources of data 
                            
                          </li>

                          <li>
                             Ability to understand different regulatory systems 
                           
                          </li>

                          <li>
                             Understanding of appraisal methodologies of different types of borrowers for secured / unsecured credit limits. 
                            
                          </li>

                          <li>
                             2+ years of experience in the credit underwriting role
                          </li>
                      </ul>
                      <br/> <br/>
                      <h5 class="font_scndhed text-left mb-0 blogHeadHome BlackColor"  style={newfront}>What would make you successful in this role</h5>
                      <div class="d-flex">
                        <ul style={{marginRight: "1rem",listStyle: "none"}} class="mt-3">
                            <li>Title</li>

                            <li>
                                Work Experience
                            </li>
                            <li>
                                Employment
                            </li>
                            <li>
                                City 
                            </li>

                            <li>
                                State / Province
                            </li>
                        </ul>

                        <ul style={{listStyle: "none"}} class="mt-3">
                            <li>Financial Specialist 
                               </li>

                            <li>
                                3 - 4 Years
                                
                            </li>
                            <li>
                                Full Time
                                
                            </li>
                            <li>
                                Mumbai
                                 
                            </li>

                            <li>
                                Maharastra
                            </li>
                        </ul>
                      </div>

                      <div class="emial_us">
                        <h5 class="font_scndhed text-left mb-0 blogHeadHome"   style={newfront}>
                           <img src={MailIcon[0].mail} alt="mail Icon" className="mailIcon"/>
                           Email us your CV to <a href="mailto:career@credfine.com" class="careermail">career@credfine.com</a></h5>
                      </div>

                     <div class="center-flex">
                      <h5 class="font_scndhed text-left uppercase blogHeadHome" style={newfront}>We love working here. you will too.</h5>
                     </div>

                     <Carousel className="careercrousal" nextIcon="" nextLabel="" prevIcon="" prevLabel="">
                    
                        <Carousel.Item>
                    <div className="crousalopportunities">
                        <div class="flex-on-crousal">
                              <div class="first_flex">
                                <h5 class="font_scndhed text-left mb-0 blogHeadHome">Endless opportunities for growth  <br/> &amp; learning!</h5>
                              <p class="mt-2 mb-0"><small class="pnt_threestke" style={{color: "white"}}>
                                I have been associated with CredFine family <br/> since 1 year
                                 and it has been the most amazing <br/> career decision till date.
                                  I love the work culture, <br/> every member here is a family,
                                   seniors are a <br/> true mentor and every day is a new learning. <br/>
                                    Thank you CredFine!  
                                    
                                    
                            </small></p>
                            <br/>
                            <p class="reena">Reena Shah, Loan Expert</p>
                              </div>
  
                              <div>
                                <img src={careerSlider[0].slide} alt=""/>
                              </div>
                            </div>
                        </div>
                        </Carousel.Item>

                        <Carousel.Item>
                    <div className="crousalopportunities">
                        <div class="flex-on-crousal">
                              <div class="first_flex">
                                <h5 class="font_scndhed text-left mb-0 blogHeadHome" style={newfront}>Endless opportunities for growth  <br/> &amp; learning!</h5>
                              <p class="mt-2 mb-0"><small class="pnt_threestke" style={{color: "white"}}>
                                I have been associated with CredFine family <br/> since 1 year
                                 and it has been the most amazing <br/> career decision till date.
                                  I love the work culture, <br/> every member here is a family,
                                   seniors are a <br/> true mentor and every day is a new learning. <br/>
                                    Thank you CredFine!  
                                    
                                    
                            </small></p>
                            <br/>
                            <p class="reena">Reena Shah, Loan Expert</p>
                              </div>
  
                              <div>
                                <img src={careerSlider[0].slide} alt=""/>
                              </div>
                            </div>
                        </div>
                        </Carousel.Item>

                        <Carousel.Item>
                    <div className="crousalopportunities">
                        <div class="flex-on-crousal">
                              <div class="first_flex">
                                <h5 class="font_scndhed text-left mb-0 blogHeadHome" style={newfront}>Endless opportunities for growth  <br/> &amp; learning!</h5>
                              <p class="mt-2 mb-0"><small class="pnt_threestke" style={{color: "white"}}>
                                I have been associated with CredFine family <br/> since 1 year
                                 and it has been the most amazing <br/> career decision till date.
                                  I love the work culture, <br/> every member here is a family,
                                   seniors are a <br/> true mentor and every day is a new learning. <br/>
                                    Thank you CredFine!  
                                    
                                    
                            </small></p>
                            <br/>
                            <p class="reena">Reena Shah, Loan Expert</p>
                              </div>
  
                              <div>
                                <img src={careerSlider[0].slide} alt=""/>
                              </div>
                            </div>
                        </div>
                        </Carousel.Item>

                        <Carousel.Item>
                    <div className="crousalopportunities">
                        <div class="flex-on-crousal">
                              <div class="first_flex">
                                <h5 class="font_scndhed text-left mb-0 blogHeadHome" style={newfront}>Endless opportunities for growth  <br/> &amp; learning!</h5>
                              <p class="mt-2 mb-0"><small class="pnt_threestke" style={{color: "white"}}>
                                I have been associated with CredFine family <br/> since 1 year
                                 and it has been the most amazing <br/> career decision till date.
                                  I love the work culture, <br/> every member here is a family,
                                   seniors are a <br/> true mentor and every day is a new learning. <br/>
                                    Thank you CredFine!  
                                    
                                    
                            </small></p>
                            <br/>
                            <p class="reena">Reena Shah, Loan Expert</p>
                              </div>
  
                              <div>
                                <img src={careerSlider[0].slide} alt=""/>
                              </div>
                            </div>
                        </div>
                        </Carousel.Item>

                        <Carousel.Item>
                        <div className="crousalopportunities">
                            <div class="flex-on-crousal">
                              <div class="first_flex">
                                <h5 class="font_scndhed text-left mb-0 blogHeadHome" style={newfront}>Endless opportunities for growth  <br/> &amp; learning!</h5>
                              <p class="mt-2 mb-0"><small class="pnt_threestke" style={{color: "white"}}>
                                I have been associated with CredFine family <br/> since 1 year
                                 and it has been the most amazing <br/> career decision till date.
                                  I love the work culture, <br/> every member here is a family,
                                   seniors are a <br/> true mentor and every day is a new learning. <br/>
                                    Thank you CredFine!  
                                    
                                    
                            </small></p>
                            <br/>
                            <p class="reena">Reena Shah, Loan Expert</p>
                              </div>
  
                              <div>
                                <img src={careerSlider[0].slide} alt=""/>
                              </div>
                            </div>
                        </div>
                        </Carousel.Item>

                    </Carousel>

                </div>

                
            </div>
            
            
            
            

        </div>
    </div>

</div>
</section>
<br/><br/>
<Footer/>


    </>)
}
export default Career;