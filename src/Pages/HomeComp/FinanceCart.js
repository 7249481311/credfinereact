import React from "react";

const FinanceCart=(props)=>{
    return(<>
         <div className="col">
             <div className="col_oninsideBack" style={props.Background}>
               
                  <div className="onColflexicon">
                    {/* <svg xmlns="http://www.w3.org/2000/svg" width="32" className="moneybagsvg" viewBox="0 0 32 32"><defs><style>.a_moneybag{fill:#bca5ff;}.b_moneybag{fill:#fff;}</style></defs><g transform="translate(-164 -438)"><circle className="a_moneybag" cx="16" cy="16" r="16" transform="translate(164 438)"></circle><g transform="translate(156.938 446)"><path className="b_moneybag" d="M28.708,12.3V9.883a6.128,6.128,0,0,0-4.041-5.754A1.408,1.408,0,0,0,24.283,2.1a2.343,2.343,0,0,0,.66-1.631A.469.469,0,0,0,24.474,0H20.708a.469.469,0,0,0-.469.469A2.343,2.343,0,0,0,20.9,2.1a1.408,1.408,0,0,0-.385,2.029,6.128,6.128,0,0,0-4.041,5.754V12.3a1.881,1.881,0,0,0,.469,3.7h11.3a1.881,1.881,0,0,0,.469-3.7ZM22.591,9.414a1.41,1.41,0,0,1,.469,2.74v.553a.469.469,0,0,1-.937,0v-.473H21.65a.469.469,0,1,1,0-.937h.941a.473.473,0,0,0,0-.945,1.41,1.41,0,0,1-.469-2.74V7.059a.469.469,0,0,1,.938,0v.473h.473a.469.469,0,0,1,0,.938h-.941a.473.473,0,0,0,0,.945Z"></path></g></g></svg> */}
                    <img src={props.LoanIcon} alt="Personal loan"/>
                    <p className="font-weight-bold mb-0 colpara">{props.LoanType} <br/>{props.Loan}</p>
                  </div>
                     <div className="padd_insidewhite">
                       <p className="classOnsmalltxt1">{props.text1}</p>
                       <p className="classOnsmalltxt1 mb-0">{props.text2}</p>
                     </div>
            </div>

           </div> 
    </>);
};

export default FinanceCart;