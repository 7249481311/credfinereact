import React from "react";
import Carousel from "react-multi-carousel";
import {ClientLogo} from "../HomeMap.js"

const LogoSlider=()=>{

    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 6,
          slidesToSlide: 3 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2,
          slidesToSlide: 2 // optional, default to 1.
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };

    return(<>
            <Carousel
  swipeable={true}
  draggable={false}
  showDots={false}
  responsive={responsive}
  infinite={true}
  autoPlay={true}
  autoPlaySpeed={1000}
  keyBoardControl={true}
  customTransition="all .5"
  transitionDuration={500}
  containerClass="carousel-container"
  removeArrowOnDeviceType={["tablet", "mobile","desktop"]}
  dotListClass="custom-dot-list-style"
  itemClass="carousel-item-padding-20-px">


  {ClientLogo.map((data,index)=>{
    return(<>
        <div key={index}><img src={data.logo1} alt="logos" width="auto"/></div>
    </>)
  })}


</Carousel>
    </>);
};
export default LogoSlider;