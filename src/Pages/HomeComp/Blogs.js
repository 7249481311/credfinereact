import React from "react";

const Blogs=(props)=>{
    return(<>
        <div className="col-md-3">
        <div className="card noneBorder blogswidth">
            <img className="card-img-top" src={props.imgsrc} alt="Card cap"/>
            <div className="card-body">
              <p className="mt-2">
              <small className="pnt_threestke meet_Team">
              {props.text1} <br/> {props.text2} <br/> {props.text3}

            </small></p>
            </div>
          </div>
      </div>
    </>);
};
export default Blogs;