import React from "react";
import AllPgsSection from "./PersonalLoanComp/AllPgsSection";
import HomeMap,{LoanPg} from "./HomeMap";
import LeadingBank from "./PersonalLoanComp/LeadingBank";
import LoanBlogs from "./PersonalLoanComp/LoanBlogs";
import Footer from "../Routing/Footer";
import Faq from "./PersonalLoanComp/Faq";

const CreditCard=()=>{

    const bottm_Margin={
        marginBottom:"0px",
    }

    return(<>
    <section>
         <div className="container-fluid credi_card_back">
          <AllPgsSection ploanIcon={HomeMap[1].LoanIcon} 
          loancard={LoanPg[1].LoanCard}
            LoanType={"Credit Cards"}
            sHead1={"Simple and transparent Comparison. Credit Card helps pay"}
            sHead2={"for what you need while building your credit."}
            text1={"Credit cards let you make high-ticket purchases on easy EMIs and"} 
            text2={"make shopping more rewarding for you. Paisabazaar is the right"}
            text3={"place to find a credit card that best fits your needs and helps you"}
            text4={"make the most of your purchases."}
            />
          </div>
</section>
<LeadingBank tabCredit={bottm_Margin}/>

<section>
  <div className="container-fluid">
    <div className="container">
      
      <div className="col-md-10 cardcol10mb">
          <div className="row">
            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Entertainment &nbsp;</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Fuel</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Travel</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Shopping</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Rewards</h4>
            </div>
          </div>

          <div className="row">
            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Secured</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Premium</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Co-Branded</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Shopping</h4>
            </div>

            <div className="col entertainmnt text-center">
                <h4 className="cal_emifnt mb-0">Cashback</h4>
            </div>
          </div>

      </div>



    </div>
    
  </div>

</section>

<section>
  
  <div className="creditcard_Blog_back">
    <LoanBlogs head="USEFUL BLOGS ON CREDIT CARDS"/>
  </div>

</section>

<section>
  <Faq/>
</section>
<Footer/>

    </>)
};
export default CreditCard;