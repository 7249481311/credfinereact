import React from "react";
import {HomeMap2,LoanPg} from "./HomeMap";
import AllPgsSection from "./PersonalLoanComp/AllPgsSection";
import LeadingBank from "./PersonalLoanComp/LeadingBank";
import LoanBlogs from "./PersonalLoanComp/LoanBlogs";
import Footer from "../Routing/Footer";
import Faq from "./PersonalLoanComp/Faq";

const LoanAgainstProperty=()=>{
    return(<>
        <section>
         <div className="container-fluid businessloan_back LoanAgainstProperty">
          <AllPgsSection ploanIcon={HomeMap2[0].LoanIcon} 
          loancard={LoanPg[4].LoanCard}
            LoanType={"Loan against Property"}
            sHead1={"Simple and transparent Comparison. Credit Card helps pay"}
            sHead2={"for what you need while building your credit."}
            text1={"Credit cards let you make high-ticket purchases on easy EMIs and"} 
            text2={"make shopping more rewarding for you. Paisabazaar is the right"}
            text3={"place to find a credit card that best fits your needs and helps you"}
            text4={"make the most of your purchases."}
            />
          </div>
        </section>

        <LeadingBank/>

        <section>
  
        <div className="ploanBlog_back" style={{backgroundColor:"#FFF0F5"}}>
            <LoanBlogs head="useful blogs on home loan"/>
        </div>

        </section>

        <section>
        <Faq/>
        </section>
        <Footer/>
    </>);
};
export default LoanAgainstProperty;