import React from "react";
import HomeMap,{LoanPg} from "./HomeMap";
import AllPgsSection from "./PersonalLoanComp/AllPgsSection";
import LoanBlogs from "./PersonalLoanComp/LoanBlogs";
import Footer from "../Routing/Footer";
import Faq from "./PersonalLoanComp/Faq";
import LeadingBank2 from "./PersonalLoanComp/LeadingBank2";

const BusinessLoan=()=>{
    return(<>
         <section>
         <div className="container-fluid businessloan_back">
          <AllPgsSection ploanIcon={HomeMap[2].LoanIcon} 
          loancard={LoanPg[2].LoanCard}
            LoanType={"Business Loan"}
            sHead1={"Simple and transparent Comparison. Credit Card helps pay"}
            sHead2={"for what you need while building your credit."}
            text1={"Credit cards let you make high-ticket purchases on easy EMIs and"} 
            text2={"make shopping more rewarding for you. Paisabazaar is the right"}
            text3={"place to find a credit card that best fits your needs and helps you"}
            text4={"make the most of your purchases."}
            />
          </div>
        </section>

        <LeadingBank2/>

        <section>
  
        <div className="ploanBlog_back">
            <LoanBlogs head="useful blogs on personal loans"/>
        </div>

        </section>

        <section>
        <Faq/>
        </section>
        <Footer/>
    </>);
};
export default BusinessLoan;