import React from "react";
import {HomeMap2,LoanPg} from "./HomeMap";
import AllPgsSection from "./PersonalLoanComp/AllPgsSection";
import LeadingBank from "./PersonalLoanComp/LeadingBank";
import LoanBlogs from "./PersonalLoanComp/LoanBlogs";
import Footer from "../Routing/Footer";
import Faq from "./PersonalLoanComp/Faq";

const InsurancePlans=()=>{
    return(<>
        <section>
         <div className="container-fluid businessloan_back InsurancePlans">
          <AllPgsSection ploanIcon={HomeMap2[2].LoanIcon} 
          loancard={LoanPg[5].LoanCard}
          homeClass={"personumbrlasvg"}
            LoanType={"Insurance Plans"}
            sHead1={"How secure are you and your family?"}
            // sHead2={"for what you need while building your credit."}
            text1={"Credit cards let you make high-ticket purchases on easy EMIs and"} 
            text2={"make shopping more rewarding for you. Paisabazaar is the right"}
            text3={"place to find a credit card that best fits your needs and helps you"}
            text4={"make the most of your purchases."}
            />
          </div>
        </section>

        <LeadingBank/>

        <section>
  
        <div className="ploanBlog_back" style={{backgroundColor:"#E4F1FF"}}>
            <LoanBlogs head="useful blogs on insurance"/>
        </div>

        </section>

        <section>
        <Faq/>
        </section>
        <Footer/>
    </>);
};
export default InsurancePlans;