import React, { useState } from "react";
import Mackbook from "../images/macbook_white.png"
import faster_processing from "../images/faster_processing.png"
import smart_compare from "../images/smart_compare.png"
import intellagence_core from "../images/intellagence_core.png"
import custmer_frndly from "../images/custmer_frndly.png"
import FinanceCart from "./HomeComp/FinanceCart";
import HomeMap,{HomeMap2} from "./HomeMap";
import LogoSlider from "./HomeComp/LogoSlider";
import {Carousel} from "react-bootstrap";
import Blogs from "./HomeComp/Blogs";
import {BlogsMap} from "./HomeMap";
import Footer from "../Routing/Footer";


const Home=()=>{

    const [tabarr1,settab1]=useState("active");
    const [tabarr2,settab2]=useState("");
    const [tabarr3,settab3]=useState("");
    const [tabarr4,settab4]=useState("");

    const [content1,setcontent1]=useState({display:"block"});
    const [content2,setcontent2]=useState({display:"none"});
    const [content3,setcontent3]=useState({display:"none"});
    const [content4,setcontent4]=useState({display:"none"});

    const [add1,setadd1]=useState("active");
    const [add2,setadd2]=useState("");
    const [add3,setadd3]=useState("");
    const [add4,setadd4]=useState("");
    const [add5,setadd5]=useState("");

    const [ApplyLoan1,setApplyLoan1]=useState({display:"block"})
    const [ApplyLoan2,setApplyLoan2]=useState({display:"none"})
    const [ApplyLoan3,setApplyLoan3]=useState({display:"none"})
    const [ApplyLoan4,setApplyLoan4]=useState({display:"none"})
    
   

    const tab1=()=>{
        settab1("active");
        settab2("");
        settab3("");
        settab4("");

        setcontent1({display:"block"})
        setcontent2({display:"none"});
        setcontent3({display:"none"});
        setcontent4({display:"none"});
    }
    const tab2=()=>{
        settab1("");
        settab2("active");
        settab3("");
        settab4("");

        setcontent1({display:"none"})
        setcontent2({display:"block"});
        setcontent3({display:"none"});
        setcontent4({display:"none"});
    }
    const tab3=()=>{
        settab1("");
        settab2("");
        settab3("active");
        settab4("");

        setcontent1({display:"none"})
        setcontent2({display:"none"});
        setcontent3({display:"block"});
        setcontent4({display:"none"});
    }
    const tab4=()=>{
        settab1("");
        settab2("");
        settab3("");
        settab4("active");

        setcontent1({display:"none"})
        setcontent2({display:"none"});
        setcontent3({display:"none"});
        setcontent4({display:"block"});
    }

    // new one progressbar

    const addone=()=>{
      setadd1("active");
      setadd2("");
      setadd3("");
      setadd4("");
      setadd5("");

      setApplyLoan1({display:"block"});
      setApplyLoan2({display:"none"});
      setApplyLoan3({display:"none"});
      setApplyLoan4({display:"none"});
      };
    const addtwo=()=>{
      setadd1("");
      setadd2("active");
      setadd3("");
      setadd4("");
      setadd5("");

      setApplyLoan1({display:"none"});
      setApplyLoan2({display:"block"});
      setApplyLoan3({display:"none"});
      setApplyLoan4({display:"none"});
      };
    const addthree=()=>{

      setadd1("");
      setadd2("");
      setadd3("active");
      setadd4("");
      setadd5("");

      setApplyLoan1({display:"none"});
      setApplyLoan2({display:"none"});
      setApplyLoan3({display:"block"});
      setApplyLoan4({display:"none"});

      };
    const addfour=()=>{
      setadd1("");
      setadd2("");
      setadd3("");
      setadd4("active");
      setadd5("");

      setApplyLoan1({display:"none"});
      setApplyLoan2({display:"none"});
      setApplyLoan3({display:"none"});
      setApplyLoan4({display:"block"});
      };

  const addfive=()=>{
    setadd1("");
      setadd2("");
      setadd3("");
      setadd4("");
      setadd5("active");

      setApplyLoan1({display:"none"});
      setApplyLoan2({display:"none"});
      setApplyLoan3({display:"none"});
      setApplyLoan4({display:"none"});
  }

    return(<>
 <section>
  
 <div className="container-fluid backOndown">
    <div className="navwidth">
       <div className="frst_head_margn">
         <h1 className="ban_heading">Finance simplified</h1>
          <p className="font-weight-bold colorOnscndgry">Fast, Secure &amp; Easy - <span className="color_pop">AI-Powered Platform</span> to get Loans, Credit Cards &amp; Insurance</p>
       </div>
    </div> 
      
      <div className="row">
        <div className="col-md-1 oncoldisnonmedia">
          
        </div>
        <div className="col-md-6 onwebpadd2">
         <div className="row">

            {HomeMap.map((data,index)=>{
                return(<>
                    <FinanceCart key={data.id} 
                    Background={data.Background}
                    LoanType={data.LoanType}
                    Loan={data.Loan} 
                    LoanIcon={data.LoanIcon}
                    text1={data.text1}
                    text2={data.text2}

                    />
                </>)
            })}

          
           </div>
            <br/>
           <div className="row">

            {HomeMap2.map((data,index)=>{
                    return(<>
                        <FinanceCart key={data.id} 
                        Background={data.Background}
                        LoanType={data.LoanType}
                        Loan={data.Loan} 
                        LoanIcon={data.LoanIcon}
                        text1={data.text1}
                        text2={data.text2}

                        />
                    </>)
                })}

           
           <div className="col"></div>

           </div>
       </div>
        <div className="col-md-5 onwebpadd">
          <img src={Mackbook} className="img-fluid mcbook_margin" alt="Mackbook" />
        </div>
      </div>

  </div>

</section>

<section>
    <div className="container">
      <h5 className="font_scndhed">CREDFINE REACH</h5>

      <div className="classOndisflex">
        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt mb-0"><span className="strokeOncr">300 Cr</span> Disbursed</h4>
            <hr className="downhrclr" />
          <p className="mb-0 italicDate"><i>in just 16 months</i></p>
        </div>

        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt mb-0"><span className="strokeOncr">7500+</span> Clients</h4> 
          <hr className="downhrclr" />
          <p className="mb-0 italicDate"><i>over 350 cities</i></p>
        </div>

        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt mb-0"><span className="strokeOncr">18</span> Banks &amp; NBFC Partners</h4>
          <hr className="downhrclr" />
          <p className="mb-0 italicDate"><i>in just 16 months</i></p>
        </div>
      </div>



      <h5 className="font_scndhed text-left">UNIQUE FEATURES</h5>

      


      <div className="flexuniqFture">
        
        <div className="clickOnchangediv">
      
          <ul className="nav-t nav-tabs clkchnge_dash">
            <li onClick={tab1} className={tabarr1}><a href="#tab1">
              <p className="text_Onchangestke frst_paddadj">Faster Loan <br/>Approval</p>
            </a></li>
            <li onClick={tab2} className={tabarr2}><a href="#tab2">
              <p className="text_Onchangestke">AI Powered <br/>Platform</p>
            </a></li>
            <li onClick={tab3} className={tabarr3}><a href="#tab3">
              <p className="text_Onchangestke">Smart <br/>Compare</p>
            </a></li>

            <li onClick={tab4} className={tabarr4}><a href="#tab4">
              <p className="text_Onchangestke lst_paddadj">Customer <br/>Friendly</p>
            </a></li>
        </ul>
        </div>


        <div className="tab-content hide" style={content1}>
          <div className="addonflxcon">
          <div className="paddIn_mid">
          <h4 className="font_changefeture">Instant Soft Approval in 5 Mins</h4>
          <p className="text_Onchangestke lineHeightmid">AI powered platform help customer know <br/>eligibility and preferred bank options in a <br/>jiffy with minimal input</p>

        </div>

        <div className="widthIn_lastdiv">
          <img src={faster_processing} className="fstprcessImg" alt="faster processing" />
        </div>


        </div>

        </div>

      <div id="tab2" className="tab-content hide" style={content2}>
        <div className="addonflxcon">
          <div className="paddIn_mid">
          <h4 className="font_changefeture">Instant Soft Approval in 5 Mins</h4>
          <p className="text_Onchangestke lineHeightmid">AI powered platform help customer know <br/>eligibility and preferred bank options in a <br/>jiffy with minimal input</p>

        </div>

        <div className="widthIn_lastdiv">
          <img src={intellagence_core} className="fstprcessImg" alt="intellagence_core" />
        </div>


        </div>

      </div>
      <div id="tab3" className="tab-content" style={content3}>

          <div className="addonflxcon">
          <div className="paddIn_mid">
          <h4 className="font_changefeture">Instant Soft Approval in 7 Mins</h4>
          <p className="text_Onchangestke lineHeightmid">AI powered platform help customer know <br/>eligibility and preferred bank options in a <br/>jiffy with minimal input</p>

        </div>

        <div className="widthIn_lastdiv">
          <img src={smart_compare} className="fstprcessImg" alt="smart compare" />
        </div>


        </div>

      </div>

      <div id="tab4" className="tab-content hide" style={content4}>

          <div className="addonflxcon">
          <div className="paddIn_mid">
          <h4 className="font_changefeture">Instant Soft Approval in 7 Mins</h4>
          <p className="text_Onchangestke lineHeightmid">AI powered platform help customer know <br/>eligibility and preferred bank options in a <br/>jiffy with minimal input</p>

        </div>

        <div className="widthIn_lastdiv">
          <img src={custmer_frndly} className="fstprcessImg" alt="customer friendly" />
        </div>


        </div>

      </div>



      </div>




    </div>
  </section>

  <section>
    <div className="container">
        
        <div className="bluecalckBack">
          <h4 className="uppercase cal_emifnt">calculate your emi and apply for loan</h4>
              <div className="button-wrap calc_tabsec">
            <p className={`clc-tb uppercase current ${add1}`} onClick={addone}>
              home loan
            </p>
            <p className={`clc-tb uppercase ${add2}`} onClick={addtwo}>
              personal loan
            </p>
            <p className={`clc-tb uppercase ${add3}`} onClick={addthree}>
             business loan
            </p>
            <p className={`clc-tb uppercase ${add4}`} onClick={addfour}>
              mortgage loan
            </p>

            <p className={`clc-tb uppercase ${add5}`} onClick={addfive}>
              balance transfer
            </p>
        </div>
  
        <div id="addone" className="showContent current" style={ApplyLoan1}>
          <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Loan amount (in lacs)</h4>

                  <div id="player">
                  <i className=""></i>
                  <div id="volume" className="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div className="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style={{width: "80%"}}></div><span className="ui-slider-handle ui-state-default ui-corner-all" tabIndex="0" style={{left: "80%"}}></span></div>
                  <i className=""></i>
                <audio id="myMedia" src="" loop="loop"></audio></div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;₹</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

             <div className="col-md-3">
               <div className="">
                <p className="mb-0 uppercase loanemi">loan emi</p>
                <p className="mb-0 loanemiPrize">40,000</p>
             </div>
           </div>

         </div>

         <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">interest rate</h4>

                  <div id="player2">
                  <i className=""></i>
                  <div id="volume2" className="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div className="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style={{width: '27%'}}></div><span className="ui-slider-handle ui-state-default ui-corner-all" tabIndex="0" style={{left: "27%"}}></span></div>
                  <i className=""></i>
                <audio id="myMedia" src="" loop="loop"></audio></div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;%</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

             <div className="col-md-3">
               <div className="">
                <p className="mb-0 uppercase loanemi">total interest</p>
                <p className="mb-0 loanemiPrize">4,45,000</p>
             </div>
           </div>

         </div>


         <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Down payment (in lacs)</h4>

                  <div id="player3">
                  <i className=""></i>
                  <div id="volume3" className="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div className="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style={{width: "12%"}}></div><span className="ui-slider-handle ui-state-default ui-corner-all" tabIndex="0" style={{left: "12%"}}></span></div>
                  <i className=""></i>
                <audio id="myMedia" src="" loop="loop"></audio></div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;₹</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

             <div className="col-md-3">
               <div className="">
                <p className="mb-0 uppercase loanemi">principal + interest</p>
                <p className="mb-0 loanemiPrize">44 lacs</p>
             </div>
           </div>

         </div>


         <div className="row">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Down payment (in lacs)</h4>

                  <div id="player4">
                  <i className=""></i>
                  <div id="volume4" className="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div className="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style={{width: "60%"}}></div><span className="ui-slider-handle ui-state-default ui-corner-all" tabIndex="0" style={{left: "60%"}}></span></div>
                  <i className=""></i>
                <audio id="myMedia" src="" loop="loop"></audio></div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;M</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

             <div className="col-md-3">
               <div className="mt-2">
                <a href="#" className="btn uppercase get_Startedbtn">get started</a>
             </div>
           </div>

         </div>


        </div>


        <div id="addtwo" className="showContent " style={ApplyLoan2}>
          <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Loan amount (in lacs)22</h4>

                  <div id="player">
                  <i className=""></i>
                  <div id="volume"></div>
                  <i className=""></i>
                </div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;₹</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

         </div>
        </div>

        <div id="addthree" className="showContent" style={ApplyLoan3}>
          <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Loan amount (in lacs)23</h4>

                  <div id="player">
                  <i className=""></i>
                  <div id="volume"></div>
                  <i className=""></i>
                </div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;₹</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

         </div>
        </div>

        <div id="addfour" className="showContent" style={ApplyLoan4}>
          <div className="row bootm_margin">
              <div className="col-md-6">
                   <h4 className="uppercase cal_prg_bar">Loan amount (in lacs)24</h4>

                  <div id="player">
                  <i className=""></i>
                  <div id="volume"></div>
                  <i className=""></i>
                </div>
              </div>

              <div className="col-md-3">
               <div className="flexdisplay mt-2">
                <span id="select-country" name="select-state" className="demo-default selectRsInr form-control store_input2 colorMinimunmOrder" required="" placeholder="Confirm your country" data-parsley-errors-container="#errors">&nbsp;₹</span>
                        

                        <input type="text" className="form-control store_input2 placeholdergraysetup inputwidthdev" aria-describedby="" placeholder="" />
                      </div>
             </div>

         </div>
        </div>

        </div>

    </div>
  </section>

  <section>
  <div className="client_Logo_slider">
     <h5 className="font_scndhed text-left uppercase mb-0">our partners</h5>
  
  <div className="brand-carousel section-padding owl-carousel owl-loaded owl-drag">
  
  <LogoSlider/>
  

  </div>
  </div>



</section>

<section>
  
  <div className="back_Onstarjuny">
    <div className="container">
        <div className="col-md-6 oncolleftpddd">
          <h5 className="font_scndhed text-left uppercase mb-0">about credfine</h5>
          <p className="mt-2"><small className="pnt_threestke">CredFine is an Indian Fintech company focused on offering range of financial <br/> products to customers and also enabling Financial Sales Partner to grow their <br/> business of Credit cards, Loans and Insurance. 
            <br/><br/>
          We offer our Financial Sales Partners suite of technology tools for end-to-end <br/> Customer Relationship Management (CRM) which is designed to ease lead<br/> management by automating time-consuming and manual processes.</small></p>


          <h5 className="font_scndhed text-left uppercase mb-0">vision</h5>

          <p className="mt-2"><small className="pnt_threestke">
            Our vision is to revolutionize the financial marketplace in India with easy <br/> access to financial products to customer as per tailormade requirement. <br/> With our Cutting-edge technology, financial expertise and realistic advice <br/> it enables the customer to take informed and right decision.
          </small></p>

          <p className="mt-2">
            <small className="pnt_threestke">
              <a href="#" className="meet_Team">Meet Our Team</a>
            </small>
          </p>

        </div>
        <div className="row">
            
            <div className="col-md-6 col_onjournypad">
              <div className="margintop_onjurny">
                <h1 className="ban_heading text-white">Start your journey <br/> with CredFine today</h1>
               <a href="#" className="btn btnaccountcrte uppercase">create account</a>
              </div>
            </div>
            <div className="col-md-6 p-2">
                  
                 <div className="padd_onahodwloan">
                  
                <Carousel>
                  <Carousel.Item>
                  <h5 className="getLoan">Simple and very smooth process <br/> easy to get loan</h5>

                    <p className="pnt_threestke expabt_tem">Inobela finanza team provides me greater <br/> support for my loan process. I found them <br/> experienced &amp; highly competent throughout <br/> the entire loan process, will recommended for <br/> others interested in applying for loan. Really <br/>appreciate their support.</p>

                    <div className="jatin_margin">
                      <p className="mb-0 pnt_threestke expabt_tem font900 pJatin">Jatin Kumar</p>
                      <p className="mb-0 pnt_threestke expabt_tem">Mumbai</p>
                      </div>
                   
                  </Carousel.Item>
                  <Carousel.Item>
                  <h5 className="getLoan">Simple and very smooth process <br/> easy to get loan</h5>

                    <p className="pnt_threestke expabt_tem">Inobela finanza team provides me greater <br/> support for my loan process. I found them <br/> experienced &amp; highly competent throughout <br/> the entire loan process, will recommended for <br/> others interested in applying for loan. Really <br/>appreciate their support.</p>

                    <div className="jatin_margin">
                      <p className="mb-0 pnt_threestke expabt_tem font900 pJatin">Jatin Kumar</p>
                      <p className="mb-0 pnt_threestke expabt_tem">Mumbai</p>
                    </div>
                  </Carousel.Item>

                  <Carousel.Item>
                  <h5 className="getLoan">Simple and very smooth process <br/> easy to get loan</h5>

                    <p className="pnt_threestke expabt_tem">Inobela finanza team provides me greater <br/> support for my loan process. I found them <br/> experienced &amp; highly competent throughout <br/> the entire loan process, will recommended for <br/> others interested in applying for loan. Really <br/>appreciate their support.</p>

                    <div className="jatin_margin">
                      <p className="mb-0 pnt_threestke expabt_tem font900 pJatin">Jatin Kumar</p>
                      <p className="mb-0 pnt_threestke expabt_tem">Mumbai</p>
                    </div>
                  </Carousel.Item>

                  <Carousel.Item>
                  <h5 className="getLoan">Simple and very smooth process <br/> easy to get loan</h5>

                    <p className="pnt_threestke expabt_tem">Inobela finanza team provides me greater <br/> support for my loan process. I found them <br/> experienced &amp; highly competent throughout <br/> the entire loan process, will recommended for <br/> others interested in applying for loan. Really <br/>appreciate their support.</p>

                    <div className="jatin_margin">
                      <p className="mb-0 pnt_threestke expabt_tem font900 pJatin">Jatin Kumar</p>
                      <p className="mb-0 pnt_threestke expabt_tem">Mumbai</p>
                    </div>
                  </Carousel.Item>

                  <Carousel.Item>
                  <h5 className="getLoan">Simple and very smooth process <br/> easy to get loan</h5>

                    <p className="pnt_threestke expabt_tem">Inobela finanza team provides me greater <br/> support for my loan process. I found them <br/> experienced &amp; highly competent throughout <br/> the entire loan process, will recommended for <br/> others interested in applying for loan. Really <br/>appreciate their support.</p>

                    <div className="jatin_margin">
                      <p className="mb-0 pnt_threestke expabt_tem font900 pJatin">Jatin Kumar</p>
                      <p className="mb-0 pnt_threestke expabt_tem">Mumbai</p>
                    </div>
                  </Carousel.Item>
                  
                </Carousel>



                 </div> 





            </div>

        </div>
    </div>
  </div>


</section>

<section>
  
  <div className="container">
    
    <div className="blogflex">
      <h5 className="font_scndhed text-left uppercase blogHeadHome">our blogs</h5>
      <p className="mb-0 viewblogfnt">View Blogs</p>
    </div>

    <div className="row">
      
      

      {BlogsMap.map((data,index)=>{
        return(<>
          <Blogs key={index} imgsrc={data.imgsrc} text1={data.text1} text2={data.text2} text3={data.text3}/>
        </>)
      })}

      
    </div>
<br/>

    <div className="blogInopptinity">
        
      <h5 className="font_scndhed uppercase">opportunities are endless</h5>

        <div className="classOndisflex addmarginoprtunity">
        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt"><span className="financefont">Finance Specialist</span></h4>
            
          <p className="mb-0 fontexp">4 - 6 Yrs Experience</p>
        </div>

        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt"><span className="financefont">Lending - Manager</span></h4> 
          
          <p className="mb-0 fontexp">4 - 6 Yrs Experience</p>
        </div>

        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt"><span className="financefont">Quality Analyst</span></h4>
          
          <p className="mb-0 fontexp">4 - 6 Yrs Experience</p>
        </div>

        <div className="mrgnInsidedlx">
          <h4 className="blueOnscndcnt"><span className="financefont">Team Lead</span></h4>
          
          <p className="mb-0 fontexp">4 - 6 Yrs Experience</p>
        </div>
      </div>

      <div className="text-center">
        <a href="#" className="btn uppercase get_Startedbtn applynowWidth">apply now</a>
      </div>

    </div>



  </div>

</section>

<section>
  <div className="container-fluid backOnLsttwo">
      
      <div className="col-md-5 m-auto">

          <div className="blogflex">
            <div className="w-100">
              <label className="signNewslter">Signup for Newsletter</label>
              <input type="email" className="form-control w-100 inputemil" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address"/>
            </div>

            <a href="#" className="btn uppercase get_Startedbtn subscrbbtn">subscribe</a>
          </div>

        </div>
      

  </div>


</section>

<Footer/>

    </>);
};
export default Home;