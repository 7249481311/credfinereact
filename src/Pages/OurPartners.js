import React from "react";
import {AboutImg1} from "./HomeMap";
import Header2 from "../Routing/Header2";
import Footer from "../Routing/Footer";
import {BankLogos1,BankLogos2,BankLogos3,BankLogos4} from "../Pages/HomeMap";
import BankPartner from "../Pages/PersonalLoanComp/BankPartner";

const OurPartners=()=>{
    return(<>
<section>
     <Header2 imgsrc1={AboutImg1[3].Img} imgsrc2={AboutImg1[4].Img}/>
</section>

<section>
    <div className="aboutContent OurTeamContent">
        <div className="container">
            <h1 className="">Partners</h1>
            <p className="mt-2"><small className="pnt_threestke">
                CredFine works with all the leading BFSI companies from India</small></p>
            <br/>
            
        </div>
    </div>

</section>

<section>
    <div className="container-fluid">
        <div className="container">
            
        </div>
    </div>
    <div className="container-fluid">
      <div className="container">
       <div className="col-md-8 pl-0">
        <h1 className="leadership2">Banks</h1>
            <div className="row">
                {BankLogos1.map((data,index)=>{
                    return(<>
                    <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                    </>)
                })}
            </div>
  
            <div className="row">
                {BankLogos2.map((data,index)=>{
                    return(<>
                    <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                    </>)
                })}
            </div>
  
            <div className="row">
                {BankLogos3.map((data,index)=>{
                return(<>
                <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                </>)
             })}
  
              <div className="col colWithlogo blueOncol" style={{visibility: 'hidden'}}>
                <div className="text-center blueInsidecol">
                  <span className="p_arrowfont rightarrp1"><i className="fas fa-long-arrow-alt-right"></i></span>
                  <span className="p_arrowfont rightarrp2"><i className="fas fa-long-arrow-alt-left"></i></span>
                </div>
  
                <p className="mb-0 listInitem text-center text-white cardcompare">Compare Cards</p>
              </div>
  
  
  
          </div>
        </div>
        
  
      </div>
      
    </div>
  
  </section>

  <section>
    <div className="container-fluid">
        <div className="container">
            
        </div>
    </div>
    <div className="container-fluid">
      <div className="container">
       <div className="col-md-8 pl-0">
        <h1 className="leadership2">Credit Cards</h1>
          <div className="row">
            {BankLogos1.map((data,index)=>{
                        return(<>
                        <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                        </>)
                    })}
  
        </div>
  
            
        </div>
        
  
      </div>
      
    </div>
  
  </section>

  <section>
    <div className="container-fluid">
        <div className="container">
            
        </div>
    </div>
    <div className="container-fluid">
      <div className="container">
       <div className="col-md-8 pl-0">
        <h1 className="leadership2">NBFCs</h1>
          <div className="row">
                   
              {BankLogos4.map((data,index)=>{
                  return(<>
                    <div className="col colWithlogo" key={index}>
                        <img src={data.Banklgo} alt="lsskss"/>
                    </div>
                  </>)
              })}
  
  
              <div className="col colWithlogo" style={{visibility: "hidden"}}></div>
  
              <div className="col colWithlogo" style={{visibility: "hidden"}}></div>
  
            </div>
  
            
        </div>
        
  
      </div>
      
    </div>
  
  </section>

  <section>
      <div class="container">
        <h1 class="leadership2">Insurance</h1>
        <div class="col-md-8 cssonShadow">
            <span class="comingsoon">Comming Soon</span>
        </div>
        
        <br/><br/>
      </div>
  </section>

  <Footer/>

    </>);
};
export default OurPartners;