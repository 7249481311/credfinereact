import React from "react"
import {AboutImg1,boymoon} from "./HomeMap";
import Header2 from "../Routing/Header2";
import Footer from "../Routing/Footer";
const AboutUs=()=>{
    return(<>
    <section>
     <Header2 imgsrc1={AboutImg1[0].Img} imgsrc2={AboutImg1[1].Img}/>
    </section>

<section>
    <div className="aboutContent">
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <h1>About Us</h1>
                    <p className="mt-2"><small className="pnt_threestke">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                        sed diam nonumy eirmod tempor invidunt ut labore et dolore 
                        magna aliquyam erat, sed diam voluptua. At vero eos et accusam 
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren, no 
                        sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum 
                        dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod 
                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                         At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
                          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy 
                          eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
                          sed diam voluptua. At vero eos et accusam et justo duo dolores 
                          et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus 
                          est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
                          consetetur sadipscing elitr
                    </small></p>

                    <br/>

                    <h1>Vision</h1>
                    <p className="mt-2"><small className="pnt_threestke">
                        CREDFINE is an Indian Fintech company focused on offering range 
                        of financial products to customers and also enabling Financial Sales 
                        Partner to grow their business of Credit cards, Loans and Insurance.
                    </small></p>

                    <br/>

                    <h1>Mission</h1>
                    <p className="mt-2"><small className="pnt_threestke">
                        CREDFINE is an Indian Fintech company focused on offering range of 
                        financial products to customers and also enabling Financial Sales Partner to grow their 
                        business of Credit cards, Loans and Insurance.
                    </small></p>
                    <br/>  <br/>
                </div>

                <div className="col-md-6">
                   <img src={boymoon[0].moonboy} alt="" className="img-fluid"/>
                </div>
            </div>
        </div>
    </div>

</section>

<Footer/>

    </>)
}
export default AboutUs;