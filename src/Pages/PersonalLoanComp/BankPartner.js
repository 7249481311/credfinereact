import React from "react";

const BankPartner=(props)=>{
    return(<>
        <div className="col colWithlogo">
              <img src={props.BankLogoImg} alt="Bank Partner"/>
        </div>
    </>);
};
export default BankPartner;