import React from "react";
import BankPartner from "./BankPartner";
import {BankLogos5,BankLogos6} from "../HomeMap";
import { useLocation } from "react-router-dom";

const LeadingBank2=(props)=>{
  
  const location=useLocation();

  if(location.pathname=="/personalLoan")
  {
    return(<>
      <section>
      <div className="container-fluid">
        <div className="container">
          <div className="col-md-8 pl-0">
            <h5 className="font_scndhed text-left uppercase mt-0 mb-2">personal loan from leading banks & partners</h5>
            <div className="row">

            {BankLogos5.map((data,index)=>{
                return(<>
                  <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                </>)
              })}

           </div>

      

        <div className="row">

        {BankLogos6.map((data,index)=>{
                return(<>
                  <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                </>)
          })}

          
      </div>
    </div>
    <div className="featureFlexMar" style={props.tabCredit}>
        <div className="d-flex">
        <i className="far fa-plus-square loanplusic"></i>
      <p className="mb-0 listInitem feature_ofc">Features and benefits of credit card</p>
      </div>

      <div className="d-flex">
        <i className="far fa-plus-square loanplusic"></i>
         <p className="mb-0 listInitem relative feature_ofc">How to use Credit Card</p>
      </div>

      <div className="d-flex">
        <i className="fas fa-minus-square loanplusic"></i>
       <p className="mb-0 listInitem relative feature_ofc">Types of Credit Cards</p>
    </div>
    </div>

  </div>
  
</div>

</section>
  </>);
  }
  else{

    return(<>
      <section>
      <div className="container-fluid">
        <div className="container">
          <div className="col-md-8 pl-0">
            <h5 className="font_scndhed text-left uppercase mt-0 mb-2">Business Loan from leading banks & partners</h5>
            <div className="row">
            <BankPartner  BankLogoImg={BankLogos5[0].Banklgo}/>
            <BankPartner  BankLogoImg={BankLogos5[2].Banklgo}/>
            <BankPartner  BankLogoImg={BankLogos5[3].Banklgo}/>
            
            <div className="col"></div>

           </div>

    </div>
    <div className="featureFlexMar" style={props.tabCredit}>
        <div className="d-flex">
        <i className="far fa-plus-square loanplusic"></i>
      <p className="mb-0 listInitem feature_ofc">Features and benefits of credit card</p>
      </div>

      <div className="d-flex">
        <i className="far fa-plus-square loanplusic"></i>
         <p className="mb-0 listInitem relative feature_ofc">How to use Credit Card</p>
      </div>

      <div className="d-flex">
        <i className="fas fa-minus-square loanplusic"></i>
       <p className="mb-0 listInitem relative feature_ofc">Types of Credit Cards</p>
    </div>
    </div>

  </div>
  
</div>

</section>
      
    </>)
  }
 
    
};
export default LeadingBank2;