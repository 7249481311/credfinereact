import React from "react";

const UsefulBlogs=(props)=>{
    return(<>
        <div className="col-md-3">
        <div className="card noneBorder ploan_B">
            <img className="card-img-top" src={props.imgsrc} alt="Card cap"/>
            <div className="card-body">
              <p className="mt-2">
              <small className="pnt_threestke meet_Team">
              {props.text1} <br/> {props.text2}

            </small></p>
            </div>
          </div>
      </div>
    </>);
}
export default UsefulBlogs;