import React from "react";
import { useLocation } from "react-router-dom";
import BusinessLoanform from "./BusinessLoanform";
import PersonalLoanform from "./PersonalLoanform";

const CompareAndApplyForm=(props)=>{

    const location = useLocation();

    if(location.pathname==`/BusinessLoan`)
    { 
    return(<>
       <BusinessLoanform FormHead={props.FormHead}/>
    </>)
}else if(location.pathname==`/personalLoan`)
{
    return(<>
        <PersonalLoanform FormHead={props.FormHead}/>
    </>)
}

};
export default CompareAndApplyForm;
