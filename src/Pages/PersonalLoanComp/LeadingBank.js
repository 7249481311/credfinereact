import React from "react";
import BankPartner from "./BankPartner";
import {BankLogos1,BankLogos2,BankLogos3} from "../HomeMap";

const LeadingBank=(props)=>{
  
    return(<>
        <section>
        <div className="container-fluid">
          <div className="container">
            <div className="col-md-8 pl-0">
              <h5 className="font_scndhed text-left uppercase mt-0 mb-2">credit cards from leading banks &amp; partners</h5>
              <div className="row">
                
                {BankLogos1.map((data,index)=>{
                  return(<>
                    <BankPartner key={index} BankLogoImg={data.Banklgo}/>
                  </>)
                })}

        </div>

          <div className="row">
          {BankLogos2.map((data,index)=>{
             return(<>
              <BankPartner key={index} BankLogoImg={data.Banklgo}/>
             </>)
           })}
           
          </div>

          <div className="row">

          {BankLogos3.map((data,index)=>{
             return(<>
              <BankPartner key={index} BankLogoImg={data.Banklgo}/>
             </>)
           })}

            

            <div className="col colWithlogo blueOncol">
              <div className="text-center blueInsidecol">
                <span className="p_arrowfont rightarrp1"><i className="fas fa-long-arrow-alt-right"></i></span>
                <span className="p_arrowfont rightarrp2"><i className="fas fa-long-arrow-alt-left"></i></span>
              </div>

              <p className="mb-0 listInitem text-center text-white cardcompare">Compare Cards</p>
            </div>



        </div>
      </div>
      <div className="featureFlexMar" style={props.tabCredit}>
          <div className="d-flex">
          <i className="far fa-plus-square loanplusic"></i>
        <p className="mb-0 listInitem feature_ofc">Features and benefits of credit card</p>
        </div>

        <div className="d-flex">
          <i className="far fa-plus-square loanplusic"></i>
           <p className="mb-0 listInitem relative feature_ofc">How to use Credit Card</p>
        </div>

        <div className="d-flex">
          <i className="fas fa-minus-square loanplusic"></i>
         <p className="mb-0 listInitem relative feature_ofc">Types of Credit Cards</p>
      </div>
      </div>

    </div>
    
  </div>

</section>
    </>);
};
export default LeadingBank;