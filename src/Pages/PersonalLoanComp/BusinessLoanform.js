import React, { useState } from "react";
import {IconImagesPNG} from "../HomeMap";
import { useForm } from "react-hook-form";

const BusinessLoanform=(props)=>{
    const [RenderState,setRenderState]=useState(true);
    const [Btname,setBtname]=useState("continue");

    const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data) =>
  {
    console.log(data);
    setRenderState(false)
     setBtname("view offers");
  }

    

    return(<>
        <div className="col-md-6">
            <div className="applyC_card newApplyCard">
              <form onSubmit={handleSubmit(onSubmit)}>
              {RenderState?<div>
                <h4 className="blueOnscndcnt dark_Bluec"><span className="font900">{props.FormHead}</span></h4>
                <hr className="bottom_border mt-0"/>
                
                <div className="FlexDisplay">
                <div className="inputDivWidth">
                
                <p className="mb-0 listInitem">Your Desired Loan Amount</p>
                
                <div className="flex-row">
                        <label className="lf--label" htmlFor="username">
                            <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                        </label>
                        <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="LoanAmount" 
                        ref={register({ required: true,pattern: /\d+/})}/>
                    </div>
                    {errors.LoanAmount && errors.LoanAmount.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                    
                    {errors.LoanAmount && errors.LoanAmount.type === "pattern" && (
                    <div className="errors">Please enter number for Loan Amount</div>
                    )}
                    
                </div>


                <div className="inputDivWidth">
                <p className="mb-0 listInitem">Gross Annual Turnover</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="GrossAnnualTurnover"
                        ref={register({ required: true })}>
                            <option value="">Select One</option>
                            <option value="Upto5Lacs">Upto 5Lacs</option>
                            <option value="5_10Lacs">5-10 Lacs</option>
                            <option value="10_25Lacs">10-25 Lacs</option>
                            <option value="25_50Lacs">25-50 Lacs</option>
                            <option value="50_75Lacs">50-75 Lacs</option>
                            <option value="75_1Cr">75-1Cr</option>
                            <option value="1_3Cr">1-3 Cr</option>
                            <option value="3_5Cr">3-5 Cr</option>
                            <option value="5Cr+">5Cr+</option>
                        </select>
                   
                    {errors.GrossAnnualTurnover && errors.GrossAnnualTurnover.type === "required" && (
                    <div className="errors">Select at least one</div>
                    )}
                   
                </div>

               

              </div>
              <br/>

              <div className="FlexDisplay">
                <div className="inputDivWidth">
                <p className="mb-0 listInitem">Net Annual Profit</p>
                <div className="flex-row">
                        <label className="lf--label" htmlFor="username">
                            <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                        </label>
                        <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="NetAnnualProfit"
                            ref={register({ required: true,pattern: /\d+/})}
                        />
                    </div>
                    
                    {errors.NetAnnualProfit && errors.NetAnnualProfit.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                    {errors.NetAnnualProfit && errors.NetAnnualProfit.type === "pattern" && (
                    <div className="errors">Please enter number for Net Annual Profit</div>
                    )}
                </div>

              

                <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Mobile Number</p>
                    <div className="flex-row">
                            <label className="lf--label" htmlFor="username">
                                +91
                            </label>
                            <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="MobileNo"
                             ref={register({ required: true,pattern: /\d+/,maxLength: 12})}
                            />
                            
                        </div>
                        {errors.MobileNo && errors.MobileNo.type === "required" && (
                            <div className="errors">This field is required</div>
                        )}

                        {errors.MobileNo && errors.MobileNo.type === "pattern" && (
                            <div className="errors">invalid Mobile Number</div>
                        )}
                        {errors.MobileNo && errors.MobileNo.type === "maxLength" && (
                            <div className="errors">invalid Mobile Number</div>
                        )}
                  </div>

              </div>
              
              <br/>
              
              <div className="FlexDisplay">
                 

                  <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Current Residence Pincode</p>
                     <input type="text" className="form-control w-100 inputemil"  aria-describedby="emailHelp" placeholder="" name="Pincode"
                         ref={register({required:true,pattern: /\d+/,minLength: 6, maxLength: 12})}
                     />
                     {errors.Pincode && errors.Pincode.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "pattern" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "minLength" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "maxLength" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                  </div>

                  <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Date of Birth</p>
                    <div className="flex-row">
                           
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">DD</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">MM</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">YY</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        {errors.dob && errors.dob.type === "required" && (
                        <div className="errors">invalid Date of Birth</div>
                        )}
                  </div>
              </div>
                <div className="checkbox checkboxdiv">
                    <label><input type="checkbox" value="" className="checkboxagree"
                    name="term_and_condition" ref={register({required:true})}
                    /><small>I have read & agreed to the Terms</small></label>
                </div>

                {errors.term_and_condition && errors.term_and_condition.type === "required" && (
                    <div className="errors">Must agree for continue</div>
                )}
              </div>:<div>
              <h4 className="blueOnscndcnt dark_Bluec"><span className="font900">{props.FormHead}</span></h4>
              <hr className="bottom_border mt-0"/>

              <div className="FlexDisplay">
                  <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Name of Business</p>
                     <input type="text" className="form-control w-100 inputemil"  placeholder="" name="NameofBusiness"
                        ref={register({ required: true})} />

                {errors.NameofBusiness && errors.NameofBusiness.type === "required" && (
                    <div className="errors">This field is required</div>
                     )}
                  </div>

                  <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Nature of Business</p>
                       <select className="form-control inputemil color_andFont cahngetogglearrw" name="NatureofBusiness"
                            ref={register({ required: true })}>
                                <option value="">Select One</option>
                                <option value="ServiceBusiness">Service Business</option>
                                <option value="Trader_Wholeseller">Trader/Wholeseller</option>
                                <option value="ManufacturingBusiness">Manufacturing Business</option>
                                <option value="Retailer">Retailer</option>
                                <option value="Others">Others</option>
                            </select>

                        {errors.NatureofBusiness && errors.NatureofBusiness.type === "required" && (
                            <div className="errors">Select at least one</div>
                            )}
                  </div>

                 
              </div>
              <br/>

              <div className="FlexDisplay">

              <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Current Business (In Years)</p>
                     <input type="text" className="form-control w-100 inputemil"  placeholder="" name="CurrentBusiness" />
                  </div>

                   <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Email ID</p>
                     <input type="email" className="form-control w-100 inputemil"  aria-describedby="emailHelp" placeholder="" name="email"
                         ref={register({required:true,pattern:/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i})}
                     />
                     {errors.email && errors.email.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}

                    {errors.email && errors.email.type === "pattern" && (
                    <div className="errors">invalid email address</div>
                    )}

                  </div>

                 

                 
              </div>

              <br/>
                
              <div className="inputDivWidth">
                       {/* <p className="mb-0 listInitem">Your Full Name</p>
                     <input type="text" className="form-control w-100 inputemil"  placeholder="" name="FullName"/> */}
                  </div>
              

              <div className="FlexDisplay">
              

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Type of Company</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="TypeofCompany">
                        <option value="">Select One</option>
                        <option value="proprietorship">proprietorship</option>
                        <option value="PartnershipFirm">Partnership Firm</option>
                        <option value="PvtLtdCompany">Pvt Ltd Company</option>
                        <option value="PublicLtdCompany">Public Ltd Company</option>
                        <option value="LLP">LLP</option>
                        <option value="SelfPracticingDoctors">Self Practicing Doctors</option>
                        <option value="SelfPracticingCA">Self Practicing CA</option>
                    </select>
                    
                </div>

                <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Current / Active EMI</p>
                  <div className="flex-row">
                            <label className="lf--label" htmlFor="username">
                                <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                            </label>
                            <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="activeEmi"/>
                        </div>
                       
                  </div>

              

              </div>

                <br/>
              <div className="FlexDisplay">
              <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Total Credit Card Outstanding</p>
                     <input type="text" className="form-control w-100 inputemil"   placeholder="" name="TotalCreditCardOutstanding"/>
                  </div>

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Commercial Property Type</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="CommercialPropertyType">
                        <option value="">Select One</option>
                        <option value="Rented">Rented</option>
                        <option value="Owned">Owned</option>
                        <option value="Leased">Leased</option>
                    </select>
                    
                </div>

                
              </div>

              <br/>
              <div className="FlexDisplay">
              <div className="inputDivWidth">
                       <p className="mb-0 listInitem">PAN NO</p>
                     <input type="text" className="form-control w-100 inputemil"   placeholder="" name="pan_no" 
                     ref={register({required:true,pattern: /\d+/})}/>
                     {errors.pan_no && errors.pan_no.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                    {errors.pan_no && errors.pan_no.type === "pattern" && (
                    <div className="errors">invalid PAN NO</div>
                    )}
                  </div>

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Bank Account Type</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="BankAccountType">
                        <option value="">Select One</option>
                        <option value="SavingAccount">Saving Account</option>
                        <option value="CurrentAccount">Current Account</option>
                        
                    </select>
                    
                </div>

                
              </div>

              <br/>
              <div className="FlexDisplay">
                     
                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Current Resident Type</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="CurrentResidentType">
                        <option value="">Select One</option>
                        <option value="Self_SpouseOwned">Self/Spouse Owned</option>
                        <option value="ParentalOwned">Parental Owned</option>
                        <option value="RentedWithFamily">Rented With Family</option>
                        <option value="RentedWithFriends">Rented With Friends</option>
                        <option value="RentedBachelor">Rented Bachelor</option>
                        <option value="PG_Hostel">PG/Hostel</option>
                        <option value="CompanyProvided">Company Provided</option>
                        
                    </select>
                    
                </div>

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">No Of years in current City</p>
                    <input type="text" className="form-control w-100 inputemil"   placeholder="" name="yearsincurrentCity"/>
                    
                </div>

                
              </div>
                <br/>
              <div className="FlexDisplay">
                     
                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Bank Account</p>
                    <input type="text" className="form-control w-100 inputemil"   placeholder="" name="BankAccount"/>
                    
                </div>

                <div className="inputDivWidth">
                   
                    
                </div>

                
              </div>
              
              
              
              

              
              </div>}

              <div className="text-center">
                <button type="button" type="submit" className="btn uppercase get_Startedbtn p_loan_getstsrt">{Btname}</button>
              </div>
              </form>
              </div>
            
          </div>
    </>)
};
export default BusinessLoanform;
