import React, { useState } from "react";
import {IconImagesPNG} from "../HomeMap";
import { useForm } from "react-hook-form";

const PersonalLoanform=(props)=>{
    const [RenderState,setRenderState]=useState(true);
    const [Btname,setBtname]=useState("continue");

    const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data) =>
  {
    console.log(data);
    setRenderState(false)
     setBtname("view offers");
  }

    

    return(<>
        <div className="col-md-6">
            <div className="applyC_card newApplyCard">
              <form onSubmit={handleSubmit(onSubmit)}>
              {RenderState?<div>
                <h4 className="blueOnscndcnt dark_Bluec"><span className="font900">{props.FormHead}</span></h4>
                <hr className="bottom_border mt-0"/>
                
                <div className="FlexDisplay">
                <div className="inputDivWidth">
                
                <p className="mb-0 listInitem">Your Desired Loan Amount</p>
                
                <div className="flex-row">
                        <label className="lf--label" htmlFor="username">
                            <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                        </label>
                        <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="LoanAmount" 
                        ref={register({ required: true,pattern: /\d+/})}/>
                    </div>
                    {errors.LoanAmount && errors.LoanAmount.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                    
                    {errors.LoanAmount && errors.LoanAmount.type === "pattern" && (
                    <div className="errors">Please enter number for Loan Amount</div>
                    )}
                    
                </div>


                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Employment Type</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="employee_type"
                    ref={register({ required: true })}>
                        <option value="">Select One</option>
                        <option value="Salaried">Salaried</option>
                        <option value="Self-Employeed">Self-Employeed</option>
                        <option value="Self-EmployeedProfessional">Self-Employeed Professional</option>
                    </select>
                    {errors.employee_type && <span className="errors">Select at least one</span>}
                </div>

              </div>
              <br/>

              <div className="FlexDisplay">
                <div className="inputDivWidth">
                <p className="mb-0 listInitem">Net Monthly Income</p>
                <div className="flex-row">
                        <label className="lf--label" htmlFor="username">
                            <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                        </label>
                        <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="NetMonthlyIncome"
                            ref={register({ required: true,pattern: /\d+/})}
                        />
                    </div>
                    
                    {errors.NetMonthlyIncome && errors.NetMonthlyIncome.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                    {errors.NetMonthlyIncome && errors.NetMonthlyIncome.type === "pattern" && (
                    <div className="errors">Please enter number for Net Monthly Income</div>
                    )}
                </div>

              

               

                  <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Date of Birth</p>
                    <div className="flex-row">
                           
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">DD</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">MM</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            <select className="form-control inputemil color_andFont cahngetogglearrw dateofBirth" name="dob"
                            ref={register({required:true})}>
                                <option value="">YY</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        {errors.dob && errors.dob.type === "required" && (
                        <div className="errors">invalid Date of Birth</div>
                        )}
                  </div>

              </div>
              
              <br/>
              
              <div className="FlexDisplay">

               <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Mobile Number</p>
                    <div className="flex-row">
                            <label className="lf--label" htmlFor="username">
                                +91
                            </label>
                            <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="MobileNo"
                             ref={register({ required: true,pattern: /\d+/,maxLength: 12})}
                            />
                            
                        </div>
                        {errors.MobileNo && errors.MobileNo.type === "required" && (
                            <div className="errors">This field is required</div>
                        )}

                        {errors.MobileNo && errors.MobileNo.type === "pattern" && (
                            <div className="errors">invalid Mobile Number</div>
                        )}
                        {errors.MobileNo && errors.MobileNo.type === "maxLength" && (
                            <div className="errors">invalid Mobile Number</div>
                        )}
                  </div>
                 

                  <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Current Residence Pincode</p>
                     <input type="text" className="form-control w-100 inputemil"  aria-describedby="emailHelp" placeholder="" name="Pincode"
                         ref={register({required:true,pattern: /\d+/,minLength: 6, maxLength: 12})}
                     />
                     {errors.Pincode && errors.Pincode.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "pattern" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "minLength" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                    {errors.Pincode && errors.Pincode.type === "maxLength" && (
                    <div className="errors">invalid Pincode</div>
                    )}

                  </div>

                  
              </div>
                <br/>

                <div className="FlexDisplay">
                <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Company Name</p>
                     <input type="text" className="form-control w-100 inputemil"  aria-describedby="emailHelp" placeholder="" name="CompanyName"
                         ref={register({required:true,})}
                     />
                     {errors.CompanyName && errors.CompanyName.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}

                  </div>
                </div>
                    

                <div className="checkbox checkboxdiv">
                    <label><input type="checkbox" value="" className="checkboxagree"
                    name="term_and_condition" ref={register({required:true})}
                    /><small>I have read & agreed to the Terms</small></label>
                </div>

                {errors.term_and_condition && errors.term_and_condition.type === "required" && (
                    <div className="errors">Must agree for continue</div>
                )}
              </div>:<div>
              <h4 className="blueOnscndcnt dark_Bluec"><span className="font900">{props.FormHead}</span></h4>
              <hr className="bottom_border mt-0"/>

              <div className="FlexDisplay">
                 

                  <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Pan Card No</p>
                     <input type="text" className="form-control w-100 inputemil"   placeholder="" name="pan_no" 
                     ref={register({required:true})}/>
                     {errors.pan_no && errors.pan_no.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}
                  </div>

                  


                  <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Total Work Eeperience</p>
                    <input type="text" className="form-control w-100 inputemil"   placeholder="" name="WorkExp"/>
                    
                </div>

              </div>
              <br/>

              <div className="FlexDisplay">

              

                  <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Current Work Experience</p>
                    <input type="text" className="form-control w-100 inputemil"   placeholder="" name="CurrentWorkExperience"/>
                    
                </div>


                   <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Email ID</p>
                     <input type="email" className="form-control w-100 inputemil"  aria-describedby="emailHelp" placeholder="" name="email"
                         ref={register({required:true,pattern:/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i})}
                     />
                     {errors.email && errors.email.type === "required" && (
                    <div className="errors">This field is required</div>
                    )}

                    {errors.email && errors.email.type === "pattern" && (
                    <div className="errors">invalid email address</div>
                    )}

                  </div>

                 

                  
              </div>

              <br/>
                
              <div className="inputDivWidth">
                       {/* <p className="mb-0 listInitem">Your Full Name</p>
                     <input type="text" className="form-control w-100 inputemil"  placeholder="" name="FullName"/> */}
                  </div>
              

              <div className="FlexDisplay">
              

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Designation</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="TypeofCompany">
                        <option value="">Select One</option>
                        <option value="">Designation</option>
                        <option value="">Designation</option>
                        <option value="">Designation</option>
                        <option value="">Designation</option>
                    </select>
                    
                </div>

                <div className="inputDivWidth">
                  <p className="mb-0 listInitem">Current / Active EMI</p>
                    <div className="flex-row">
                            <label className="lf--label" htmlFor="username">
                                <img src={IconImagesPNG[0].IndianINR} alt="indian rupies symbol" className="rupiesSymbol"/>
                            </label>
                            <input  className='lf--input inputemil color_andFont' placeholder='' type='text' name="activeEmi"/>
                        </div>
                       
                  </div>

                

              </div>

                <br/>
              <div className="FlexDisplay">
              <div className="inputDivWidth">
                       <p className="mb-0 listInitem">Total Credit Card Outstanding</p>
                     <input type="text" className="form-control w-100 inputemil"   placeholder="" name="TotalCreditCardOutstanding"/>
                  </div>

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Mode Of Salary Credit</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="ModeOfSalaryCredit"
                    ref={register({required:true})}>
                        <option value="">Select One</option>
                        <option value="BankTransfer">Bank Transfer</option>
                        <option value="ChequeTransfer">Cheque Transfer</option>
                        <option value="CashSalary">Cash Salary</option>
                    </select>

                    {errors.ModeOfSalaryCredit && errors.ModeOfSalaryCredit.type === "required" && (
                    <div className="errors">Select at least one</div>
                    )}
                    
                </div>

                
              </div>

              <br/>
              <div className="FlexDisplay">
              

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Salary Account Bank</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="SalaryAccountBank"
                    ref={register({required:true})}>
                        <option value="">Select One</option>
                        <option value="HDFC">HDFC</option>
                        <option value="ICICI">ICICI</option>
                        <option value="KotakMahindraBank">Kotak Mahindra Bank</option>
                        <option value="SCB">SCB</option>
                        <option value="SBI">SBI</option>
                        <option value="PNB">PNB</option>
                        <option value="IDFC">IDFC</option>
                        <option value="CBI">CBI</option>
                        <option value="BOB">BOB</option>
                        <option value="IndianBank">Indian Bank</option>
                        <option value="BankOfIndia">Bank Of India</option>
                        <option value="YesBank">Yes Bank</option>
                        <option value="CitiBank">Citi Bank</option>
                        <option value="BankOfMaharashtra">Bank Of Maharashtra</option>
                        <option value="OBC">OBC</option>
                        <option value="IndusindBank">Indusind Bank</option>
                        <option value="HSBC">HSBC</option>
                        <option value="DBS">DBS</option>
                        <option value="UcoBank">Uco Bank</option>
                        <option value="Axisbank">Axis bank</option>
                        
                    </select>
                    {errors.SalaryAccountBank && errors.SalaryAccountBank.type === "required" && (
                    <div className="errors">Select at least one</div>
                    )}
                </div>

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">Current Resident Type</p>
                    <select className="form-control inputemil color_andFont cahngetogglearrw" name="CurrentResidentType">
                        <option value="">Select One</option>
                        <option value="Self_SpouseOwned">Self/Spouse Owned</option>
                        <option value="ParentalOwned">Parental Owned</option>
                        <option value="RentedWithFamily">Rented With Family</option>
                        <option value="RentedWithFriends">Rented With Friends</option>
                        <option value="RentedBachelor">Rented Bachelor</option>
                        <option value="PG_Hostel">PG/Hostel</option>
                        <option value="CompanyProvided">Company Provided</option>
                        
                    </select>
                    
                </div>

                
              </div>

              <br/>
              <div className="FlexDisplay">
                     
                

                <div className="inputDivWidth">
                    <p className="mb-0 listInitem">No Of years in current City</p>
                    <input  className='lf--input inputemil color_andFont w-100' placeholder='' type='text' name="yearInC_City"/>
                    
                </div>

                <div className="inputDivWidth">
                   
                    
                   </div>

                
              </div>
             
              
              </div>}

              <div className="text-center">
                <button type="button" type="submit" className="btn uppercase get_Startedbtn p_loan_getstsrt">{Btname}</button>
              </div>
              </form>
              </div>
            
          </div>
    </>)
};
export default PersonalLoanform;
