import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import ApplyCardForm from "./ApplyCardForm";
import CompareAndApplyForm from "./CompareAndApplyForm";

const AllPgsSection=(props)=>{

  const location=useLocation();
  const [Buisnessloan,setBuisnessloan]=useState("Compare and Apply for Personal Loans")

  // console.log(location);

  useEffect(()=>{

    if(location.pathname==`/BusinessLoan`)
    {
      setBuisnessloan("Compare and Apply for Business Loans");
    }

  })


    return(<>
          <div className="container">
             <div className="topBootom_mrgn">
          <div className="row">
            

          <div className="col-md-6">
            <div className="ploanflex">

              <img src={props.ploanIcon} alt="personal Loan"/>
               
              <h2 className="ploanHead">{props.LoanType}</h2>
            </div>

            <div>
              <p className="pnt_threestke expabt_tem mt-2">{props.sHead1} <br/> {props.sHead2}</p>

               <div>

                <img src={props.loancard} alt="Personal loan card" className={`rupiescardsvg ${props.homeClass}`}/>
                  
               </div>



                <h5 className="font_scndhed text-left uppercase mb-0">Compare &amp; Apply for Credit Cards on credfine</h5>

                <p className="mt-2"><small className="pnt_threestke">
                {props.text1} <br/> {props.text2} <br/> {props.text3} <br/> {props.text4} <a href="">Read FAQs</a>
              </small></p>

            </div>


          </div>

         
          {location.pathname==`/personalLoan`||location.pathname==`/BusinessLoan`?<CompareAndApplyForm FormHead={Buisnessloan}/>:<ApplyCardForm/>}
          

      

        </div>
        </div>
      </div>
    </>);
};
export default AllPgsSection;