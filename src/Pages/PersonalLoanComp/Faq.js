import React from "react";

const Faq=()=>{
    return(<>
        <div className="container">
    <br/><br/>
    <div className="col-md-9 oncol7padd">
      <div className="mar_on_l">
       <div className="flex_onmarginadj">
       
          <i className="fas fa-minus-square loanplusic"></i>
        <h4 className="fontfaq_lon">FAQs</h4>
       </div>
      </div>

      <h5 className="font_scndhed text-left mb-0 mt-2 blogHeadHome">1. What is a Credit Card?</h5>

      <p className="mt-1"><small className="pnt_threestke nvyblue">
               Credit card is a convenient alternative to cash or check-based transactions issued by banks and financial institutions, enabling the holder to make a purchase (or draw advance cash) on credit. The due amount at the end of the billing cycle may be paid as a lump sum within the due date in smaller instalments before the final due date. The purchases that are made using a credit card are in fact short term unsecured microloans and if paid off within a specified time (interest-free period) do not have any interest payouts. Additionally, credit cards allow you to benefit from the option of revolving credit i.e. you might pay off your credit card dues in one go or make multiple small payments to pay off your debt. Also known as plastic money, credit cards have only recently been introduced in India as compared to the western world. According to RBI data, as of December 2015, there were 2.27 crore credit cards in circulation within India. Even better, you can use credit cards not just within India but also worldwide. 
    </small></p>

    <p className="mt-2 mb-2"><small className="pnt_threestke nvyblue">
      Credit cards are in fact tools that allow you to pay for goods and services using the bank’s money instead of your own. But this is only for a short period of time because if you fail to pay back the money before the end of your interest-free period, you will be charged interest on the amount owed to the bank.
    </small></p>

    <br/>

     <h5 className="font_scndhed text-left mb-0 blogHeadHome">2. Different between credit card and debit card?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
      The money that you draw out via debit card is your own money which is debited from the bank account linked to your card. On the other hand, the money that you spend (purchases) or withdraw (cash advance) via a credit card is more like a short term loan granted by card issuing entity. Instead of paying off this short term loan on a per use basis, all your expenses get converted into a consolidated bill at the end of the billing cycle.
    </small></p>

    <br/>

     <h5 className="font_scndhed text-left mb-0 blogHeadHome">3. What is the CVV number?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
     Visa and MasterCard are global payment technology companies based out of the US that connects consumers, businesses, banks and governments across more than 200 countries and territories, enabling them to use digital currency instead of cash, checks, drafts, etc. Apart from VISA and MasterCard, other leading global payment technology companies are American Express, Diners Club and Discover.
    </small></p>
    <br/>
     <h5 className="font_scndhed text-left mb-0 blogHeadHome">4. What is the eligibility criteria for availing credit card?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
     Any credit card that features a merchant’s logo/name as a co-promoter of the specific credit card along with the card issuer is termed as a co-branded credit card. Leading merchants in India who sponsor co-branded credit cards include airlines, online shopping websites and others.
    </small></p>

    <br/>

    <h5 className="font_scndhed text-left mb-0 blogHeadHome">5. Does bank charge amc on credit card?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
     EMV – which is short for Europay, MasterCard and Visa — is a global standard for cards equipped with the chip and PIN technology. The chip card and PIN code need to be used together to authenticate chip-card transactions. Unlike magnetic-stripe cards, every time an EMV card is used for payment, the card chip creates a unique transaction code that cannot be used for another transaction. EMV technology provides an additional level of security in point of sales (POS) transactions to prevent credit card frauds.
    </small></p>

    <br/>

    <h5 className="font_scndhed text-left mb-0 blogHeadHome">6. What is the minimum payment due?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
       The CVV Number (Card Verification Value) it is a 3 digit number mentioned on the back side of your credit card on the right side of the signature panel. Also known as the CVV2, this 3 digit number is an essential part of the verification process especially in case of online transactions. The CVV number should not be disclosed to anyone.  
    </small></p>

    <br/>

     <h5 className="font_scndhed text-left mb-0 blogHeadHome">7. Can i Transfer my credit card outstanding into personal loan?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
       A supplementary/add-on card refers to one or more credit cards that may be issued to family members of the primary cardholder. Add on cards share the total credit limit of the primary card account and have the same features as the primary card. Any reward points earned through the add-on card(s) are credited to the primary card account. 
    </small></p>

    <br/>

     <h5 className="font_scndhed text-left mb-0 blogHeadHome">8. Type of card?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
       Though this tends to vary from one credit card issuer to another, in most cases, you can apply for up to 3 supplementary cards for family members like your spouse, children, parents and siblings.
    </small></p>

    <br/>

    <h5 className="font_scndhed text-left mb-0 blogHeadHome">9. What is late payment fee</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
      Though this tends to vary from one credit card issuer to another, in most cases, you can apply for up to 3 supplementary cards for family members like your spouse, children, parents and siblings.
    </small></p>

    <br/>

     <h5 className="font_scndhed text-left mb-0 blogHeadHome">10. How much interest does bank charge on monthly basis?</h5>
     <p className="mt-2"><small className="pnt_threestke nvyblue">
      Though this tends to vary from one credit card issuer to another, in most cases, you can apply for up to 3 supplementary cards for family members like your spouse, children, parents and siblings.
    </small></p>



      <div className="bestc_bstoff">
        <h4 className="blueOnscndcnt"><span className="bestcardOff nvyblue mb-0">Best Cards &amp; Offers from CredFine</span></h4>
        <a href="#" className="btn uppercase get_Startedbtn getploan2ndbtn">get started</a>
      </div>



    </div>
  </div>
    </>);
};
export default Faq;