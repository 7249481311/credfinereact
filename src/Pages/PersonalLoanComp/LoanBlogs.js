import React from "react";
import UsefulBlogs from "./UsefulBlogs";
import {BlogPLoan} from "../HomeMap";

const LoanBlogs=(props)=>{
    return(<>
        <div className="container">
    
    <div className="blogflex">
      <h5 className="font_scndhed text-left uppercase blogHeadHome">{props.head}</h5>
    </div>

    <div className="row">
      
        {BlogPLoan.map((data,index)=>{
          return(<>
            <UsefulBlogs key={index} imgsrc={data.BpLoan} text1={data.text1} text2={data.text2}/>
          </>)
        })}
      

      


      <div className="col-md-3">
        <div className="card noneBorder ploan_B">
           
            <div className="card-body">
              <div className="body_oncard_margin">
                <h4 className="blueOnscndcnt mb-0 d-flex"><span className="font700">View all Blogs</span>  <span className="allBview"><i className="fas fa-long-arrow-alt-right"></i></span></h4>
              </div>
            </div>
          </div>
      </div>
      
      
    </div>

  </div>
    </>);
}
export default LoanBlogs;