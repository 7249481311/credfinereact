import LoanIcon1 from "../images/homeSvg/1.svg";
import LoanIcon2 from "../images/homeSvg/2.svg";
import LoanIcon3 from "../images/homeSvg/3.svg";
import LoanIcon4 from "../images/homeSvg/4.svg";
import LoanIcon5 from "../images/homeSvg/5.svg";
import LoanIcon6 from "../images/homeSvg/6.svg";
import LoanIcon7 from "../images/homeSvg/7.svg";
import Clogo1 from "../images/client_logo/client_1.png";
import Clogo2 from "../images/client_logo/client_2.png";
import Clogo3 from "../images/client_logo/client_3.png";
import Clogo4 from "../images/client_logo/client_4.png";
import Clogo5 from "../images/client_logo/client_5.png";
import Clogo6 from "../images/client_logo/client_6.png";
import BlogImg1 from "../images/blog1.png";
import BlogImg2 from "../images/blog2.png";
import BlogImg3 from "../images/blog3.png";
import BlogImg4 from "../images/blog4.png";
import ftrSvg1 from "../images/footerSvg/1twitter.svg"
import ftrSvg2 from "../images/footerSvg/2linkedin.svg"
import ftrSvg3 from "../images/footerSvg/3facebook.svg"
import ftrSvg4 from "../images/footerSvg/4instagram.svg"
import LoanCard1 from "../images/personal_loan/svg/Personal_loan.svg"
import LoanCard2 from "../images/personal_loan/svg/credit_card.svg"
import LoanCard3 from "../images/personal_loan/svg/businessLoan.svg"
import LoanCard4 from "../images/personal_loan/svg/homeLoan.svg"
import LoanCard5 from "../images/personal_loan/svg/LoanAgainstProperty.svg"
import LoanCard6 from "../images/personal_loan/svg/insurancePlans.svg";
import LoanCard7 from "../images/personal_loan/balanceTransfer.png";
import Banklogo1 from "../images/personal_loan/bankk1.png";
import Banklogo2 from "../images/personal_loan/bankk2.png";
import Banklogo3 from "../images/personal_loan/bankk3.png";
import Banklogo4 from "../images/personal_loan/bankk4.png";
import Banklogo5 from "../images/personal_loan/bankk5.png";
import Banklogo6 from "../images/personal_loan/bankk6.png";
import Banklogo7 from "../images/personal_loan/bankk7.png";
import Banklogo8 from "../images/personal_loan/bankk8.png";
import Banklogo9 from "../images/personal_loan/bankk9.png";
import Banklogo10 from "../images/personal_loan/bankk10.png";
import Banklogo11 from "../images/personal_loan/bankk11.png";
import Banklogo12 from "../images/personal_loan/bank12.png";
import Banklogo13 from "../images/personal_loan/bank13.png";
import Banklogo14 from "../images/personal_loan/bank_logo2/bank1.png";
import Banklogo15 from "../images/personal_loan/bank_logo2/bank2.png";
import Banklogo16 from "../images/personal_loan/bank_logo2/bank3.png";
import Banklogo17 from "../images/personal_loan/bank_logo2/bank4.png";
import Banklogo18 from "../images/personal_loan/bank_logo2/bank5.png";
import Banklogo19 from "../images/personal_loan/bank_logo2/bank6.png";
import blogPloan1 from "../images/personal_loan/blog_plone1.png";
import blogPloan2 from "../images/personal_loan/blog_plone2.png";
import blogPloan3 from "../images/personal_loan/blog_plone3.png";
import aboutImag1 from "../images/about/1.png";
import aboutImag2 from "../images/about/2.png";
import aboutImag3 from "../images/about/3.png";
import aboutImag4 from "../images/about/5.png";
import aboutImag5 from "../images/about/6.png";
import aboutImag6 from "../images/about/4.png";
import aboutImag7 from "../images/about/7.png";
import moonboy from "../images/boymoon.png";
import team1 from "../images/team/1.png";
import team2 from "../images/team/2.png";
import team3 from "../images/team/3.png";
import team4 from "../images/team/4.png";
import team5 from "../images/team/5.png";
import team6 from "../images/team/6.png";
import team7 from "../images/team/7.png";
import team8 from "../images/team/8.png";
import team9 from "../images/team/9.png";
import linkedinsvg from "../images/svgicon/linkedin.svg";
import twittersvg from "../images/svgicon/twitter.svg";
import iconmail from "../images/svgicon/mail.svg";
import slide1 from "../images/slide1.png";
import logo from "../images/logoSvg/logo.svg";
import IndianINR from "../images/svgicon/indian_rupee_sign.png";

const HomeMap=[
    {
        id:1,
        Background:{background:"#F3EFFF"},
        LoanIcon:LoanIcon1,
        LoanType:"Personal",
        Loan:"Loan",
        text1:"Lowest ROI Starting from 10.45%",
        text2:"Lowest Per lakh EMI of ₹ 2147 for 5 years"
    },

    {
        id:2,
        Background:{background:"#FDEEE5"},
        LoanIcon:LoanIcon2,
        LoanType:"Credit",
        Loan:"Card",
        text1:"AMC Waived",
        
    },

    {
        id:3,
        Background:{background:"#E2E7FF"},
        LoanIcon:LoanIcon3,
        LoanType:"Business",
        Loan:"Loan",
        text1:"Lowest ROI Starting from 13.50%",
        text2:"Lowest EMI Per Lakh of ₹ 2301 for 5 years"
    },

    {
        id:3,
        Background:{background:"#DCF8F8"},
        LoanIcon:LoanIcon4,
        LoanType:"Home",
        Loan:"Loan",
        text1:"Lowest ROI Starting from 6.95%",
        text2:"Lowest EMI Per lakh of ₹ 662 for 30 years"
    },

];

const HomeMap2=[
    {
        id:1,
        Background:{background:"#FCEBF0"},
        LoanIcon:LoanIcon5,
        LoanType:"Loan Against",
        Loan:"Property",
        text1:"Lowest ROI Starting from 7.75%",
        text2:"Lowest EMI Per lakh of ₹ 821 for 20 years"
    },
    {
        id:2,
        Background:{background:"#DCF8F8"},
        LoanIcon:LoanIcon6,
        LoanType:"Balance",
        Loan:"Transfer",
        text1:"Transfer loan - Save on your existing EMI",
        text2:"Avail additional loan by transfer of existing loan"
    },

    {
        id:3,
        Background:{background:"#E4F1FF"},
        LoanIcon:LoanIcon7,
        LoanType:"Insurance",
        Loan:"Plans",
        text1:"Covid-19 Plans with lowest premium",
        text2:"1 Cr Term plan at ₹ 200 per month"
    },

];

const ClientLogo=[
    {
        logo1:Clogo1,
    },
    {
        logo1:Clogo2,
    },

    {
        logo1:Clogo3,
    },

    {
        logo1:Clogo4,
    },
    {
        logo1:Clogo5,
    },
    {
        logo1:Clogo6,
    },
    {
        logo1:Clogo1,
    },
    {
        logo1:Clogo2,
    },

    {
        logo1:Clogo3,
    },

    {
        logo1:Clogo4,
    },
    {
        logo1:Clogo5,
    },
    {
        logo1:Clogo6,
    },

];

const BlogsMap=[
    {
        imgsrc:BlogImg1,
        text1:"How Does One Late",
        text2:"Payment Affect Your",
        text3:"Credit Score?",
    },
    {
        imgsrc:BlogImg2,
        text1:"Financial Tips for",
        text2:"Those Affected by",
        text3:"Coronavirus",
    },
    {
        imgsrc:BlogImg3,
        text1:"Prepare for the",
        text2:"Unexpected with an",
        text3:"Emergency Fund",
    },
    {
        imgsrc:BlogImg4,
        text1:"Small Financial Decisions You",
        text2:"Can Make Today to Secure",
        text3:"Your Future",
    },
   
];

const FooterSvg=[
    {
        svg:ftrSvg1,
    },
    {
        svg:ftrSvg2,
    },
    {
        svg:ftrSvg3,
    },
    {
        svg:ftrSvg4,
    },
];

const LoanPg=[
    {
        LoanCard:LoanCard1,
    },
    {
        LoanCard:LoanCard2,
    },
    {
        LoanCard:LoanCard3,
    },
    {
        LoanCard:LoanCard4,
    },
    {
        LoanCard:LoanCard5,
    },
    {
        LoanCard:LoanCard6,
    },
    {
        LoanCard:LoanCard7,
    }
];
const BankLogos1=[
    {
        Banklgo:Banklogo1,
    },
    {
        Banklgo:Banklogo2,
    },
    {
        Banklgo:Banklogo3,
    },
    {
        Banklgo:Banklogo4,
    },
   
    
];

const BankLogos2=[
    {
        Banklgo:Banklogo5,
    },
    {
        Banklgo:Banklogo6,
    },
    {
        Banklgo:Banklogo7,
    },
    {
        Banklgo:Banklogo8,
    },
];

const BankLogos3=[
    {
        Banklgo:Banklogo9,
    },
    {
        Banklgo:Banklogo10,
    },
    {
        Banklgo:Banklogo11,
    },
];

const BankLogos4=[
    {
        Banklgo:Banklogo12,
    },
    {
        Banklgo:Banklogo13,
    },
];

const BankLogos5=[
    {
        Banklgo:Banklogo14,
    },
    {
        Banklgo:Banklogo15,
    },
    {
        Banklgo:Banklogo16,
    },
    {
        Banklgo:Banklogo17,
    },
];

const BankLogos6=[
    {
        Banklgo:Banklogo18,
    },
    {
        Banklgo:Banklogo5,
    },
    {
        Banklgo:Banklogo9,
    },
    {
        Banklgo:Banklogo19,
    },
    
];



const BlogPLoan=[
    {
        BpLoan:blogPloan1,
        text1:"How to Lower Your Credit Card",
        text2:"Interest Rate",
    },
    {
        BpLoan:blogPloan2,
        text1:"Eight Tips to Make Credit Cards",
        text2:"Work for You, Not Against You",
    },
    {
        BpLoan:blogPloan3,
        text1:"When Paying Bills With Credit Card",
        text2:"Makes Sense (and When It Doesn’t)",
    },
];

const AboutImg1=[
    {
      Img:aboutImag1,
    },
    {
        Img:aboutImag2,
    },
    {
        Img:aboutImag3,
    },
    {
        Img:aboutImag4,
    },
    {
        Img:aboutImag5,
    },
    {
        Img:aboutImag6,
    },
    {
        Img:aboutImag7,
    },
];
const boymoon=[
    {
        moonboy:moonboy,
    }
];

const TeamImage=[
    {
        TeamImg:team1,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team2,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team3,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },

];

const TeamImage2=[
    {
        TeamImg:team4,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team5,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team6,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },

];

const TeamImage3=[
    {
        TeamImg:team7,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team8,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },
    {
        TeamImg:team9,
        linkedin:linkedinsvg,
        twitter:twittersvg,
    },

];

const MailIcon=[
    {
        mail:iconmail,
    }
];

const careerSlider=[
    {
        slide:slide1,
    },
    
];

const credfine=[
    {
        logoCredfine:logo,
    },
];

const IconImagesPNG=[
    {
        IndianINR:IndianINR
    },
]



export default HomeMap;
export {HomeMap2,ClientLogo,BankLogos5,BankLogos6,BlogsMap,FooterSvg,LoanPg,BankLogos1,BankLogos2,BankLogos3,BankLogos4,BlogPLoan,AboutImg1,boymoon,TeamImage,TeamImage2,TeamImage3,MailIcon,careerSlider,credfine,IconImagesPNG}
